﻿using System;
using System.Collections.Generic;
using System.Linq;
using ERP.INFRASTRUCTURE.Entity;

namespace ERP.INFRASTRUCTURE.Global
{
    public class LogInInfo
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public long TemplateId { get; set; }
        public string TemplateName { get; set; }
        public long ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public List<PartialPermission> Permissions { get; set; }
        public bool Islogin { get; set; }
        public string IpAddress { get; set; }
        public LogInInfo() { }
    }
}