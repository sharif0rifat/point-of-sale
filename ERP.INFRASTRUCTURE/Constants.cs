﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.INFRASTRUCTURE
{
    public static class Constants
    {
        public const string EmptyString = "-1";

        public enum PartyType
        {
            SupplierParty = 1, DealerParty = 2
        }
        public enum TransactionType
        {
            Receive = 1, Payment = 2, RecievedReturn, PaymentReturn
        }
        public enum HeaderType
        {
            Debit = 1, Credit = 2
        }
        public enum SalaryType
        {
            Current = 1, Advance = 2
        }
        public enum LedgerType
        {
            CashTransaction = 1, PurchaseLedger, SalesLedger, SalaryLedger, StockLedger, ProfitLoss, Capital
        }
        public enum AccountsTytpe
        {
            Asset = 1, Liability, Equity, PositiveEquity, NegativeEquity
        }
        public enum PurchaseInvoiceType
        {
            Purchase = 1,
            PurchaseReturn,
            Lost,
            Profit
        }
        public enum SalesInvoiceType
        {
            Sales=1,
            SalesReplace, 
            SalesReturn,
        }

        public enum Payment
        {
            Paid = 1,
            Pending
        }
    }
}
