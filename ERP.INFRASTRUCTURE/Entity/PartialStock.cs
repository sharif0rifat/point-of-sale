﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ERP.INFRASTRUCTURE
{
    public class StockViewModel
    {
        [Display(Name = "Category Name")]
        public long CategoryId { get; set; }
        public long ProductId { get; set; }
        [Display(Name = "Purchase")]
        public long PurchaseId { get; set; }
        [Range(typeof(decimal), "0", "9999999999")]
        public decimal Amount { get; set; }
        [UIHint("DateTime")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Stock Date")]
        public DateTime PurchasekDate { get; set; }
        public bool IsEdit { get; set; }
         [Display(Name = "Model Name")]
        public string ProductName { get; set; }
        [Display(Name = "MPI/Mura No.")] 
        public string Invoice { get; set; }
        public int? PurchaseType { get; set; }
        [Range(typeof(long), "0", "9999999999")]
        public decimal Quantity { get; set; }
        public decimal BuyngPrice { get; set; }
        public decimal SellingPrice { get; set; }
        public decimal Price { get; set; }
    }
}
