
namespace ERP.INFRASTRUCTURE
{
    using System;
    using System.Collections.Generic;
    
    public class TableProperty
    {
        public long Id { get; set; }
        public string TableName { get; set; }
        public string PropertyName { get; set; }
        public long PropertyValue { get; set; }
    }
}
