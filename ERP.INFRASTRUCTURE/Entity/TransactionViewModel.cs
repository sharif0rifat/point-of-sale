using ERP.INFRASTRUCTURE.Global;
using System;
using System.ComponentModel.DataAnnotations;

namespace ERP.INFRASTRUCTURE.Entity
{
    public class TransactionViewModel
    {
        public long Id { get; set; }
        public long CreatedBy { get; set; }
        public long ModifiedBy { get; set; }
        public long PartyId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int Status { get; set; }
        public string BussinessId { get; set; }
        public long DebitHeaderId { get; set; }
        public string DebitHeaderName { get; set; }
        public long CreditHeaderId { get; set; }
        public string CreditHeaderName { get; set; }
        public decimal? Amount { get; set; }
        public decimal CreditAmount { get; set; }
        public decimal DebitAmount { get; set; }
        //[DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime TransactionDate { get; set; }
        public string Remarks { get; set; }
        public LogInInfo LogInInfo { get; set; }
        public int CurrencyId { get; set; }
    }
}
