﻿using System.Collections.Generic;

namespace ERP.INFRASTRUCTURE
{
    public class DashBoard
    {
        public int ActiveBranch { get; set; }
        public int TodayClientRegistered { get; set; }
        public int ClientServed { get; set; }
        public int ActiveUser { get; set; }
    }
}
