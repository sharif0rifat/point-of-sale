
using System.Globalization;

namespace ERP.INFRASTRUCTURE
{
    using System;
    using System.Collections.Generic;
    
    public class Salary
    {
        public long Id { get; set; }
        public long CreatedBy { get; set; }
        public long ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int Status { get; set; }
        public long BussinessId { get; set; }
        public long EmployeeId { get; set; }
        public Nullable<long> SalryType { get; set; }
        public string SalaryTypeName { get; set; }
        public Nullable<System.DateTime> Month { get; set; }
        public Nullable<decimal> TotalSalary { get; set; }
        public Nullable<decimal> BasicSalary { get; set; }
        public Nullable<decimal> HouseRent { get; set; }
        public Nullable<decimal> MobileAllowance { get; set; }
        public Nullable<decimal> TransportCost { get; set; }
        public Nullable<decimal> FestivalBonus { get; set; }
        public Nullable<decimal> Commission { get; set; }
        public Nullable<decimal> TotalBill { get; set; }
        public Nullable<System.DateTime> CommissionFromDate { get; set; }
        public Nullable<System.DateTime> CommissionToDate { get; set; }
        public Nullable<decimal> MedicalAllowance { get; set; }
        public Nullable<decimal> OtherAllowance { get; set; }
        public Nullable<decimal> OtherDeduction { get; set; }
        public string EmployeeName { get; set; }
        public string MonthName {
            get { return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Month.Value.Month); }
        }
        public bool IsEdit { get; set; }
        public long DebitId { get; set; }
        public long CreditId { get; set; }
    }
}
