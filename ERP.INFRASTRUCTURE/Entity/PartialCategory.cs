using System;

namespace ERP.INFRASTRUCTURE.Entity
{
    public class PartialCategory
    {
        public long Id { get; set; }
        public long CreatedBy { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int Status { get; set; }
        public long BussinessId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsParent { get; set; }
        public long? ParentId { get; set; }
        public bool IsEdit { get; set; }
    }
}
