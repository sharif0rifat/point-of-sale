using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ERP.INFRASTRUCTURE
{
    using System;

    public class PartialInvoiceInfo
    {
        public long Id { get; set; }
        public long CreatedBy { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int Status { get; set; }
        public long BussinessId { get; set; }
        public string InvoiceNo { get; set; }
        [Required]
        [Range(-1, int.MaxValue, ErrorMessage = "You Must select a party")]
        public long PartyId { get; set; }
        public string PartyName { get; set; }
        public string PartyAddress { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "You Must give a valid amount.")]
        public decimal SubTotal { get; set; }
        public decimal GrandTotal { get; set; }
        public long? InvoiceReferenceId { get; set; }
        public long PropertyId { get; set; }
        public decimal? ReturnTotal { get; set; }
        public string Remarks { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public IList<InvoiceDetailViewModel> InvoiceDetailsList { get; set; }
        public bool IsEdit { get; set; }
        public string CreatedByName { get; set; }
        public string PropertyName { get; set; }
        public decimal Vat { get; set; }
        public decimal Discount { get; set; }
        public long DebitId { get; set; }
        public long CreditId { get; set; }
        public int CurrencyId { get; set; }
        public decimal ServiceCharge { get; set; }
        public long ProcessBy { get; set; }
        public long ApprovedBy { get; set; }
        public string ReferenceNo { get; set; }
        public decimal ConversionRate { get; set; }
        public long Currency { get; set; }
        public long ProductId { get; set; }
    }
}
