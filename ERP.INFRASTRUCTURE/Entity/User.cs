﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ERP.INFRASTRUCTURE
{
    public class User
    {
        public long Id { get; set; }
        [Required]
        [Display(Name = "User Id")]
        [RegularExpression(@"^\S+$", ErrorMessage = "white space is not allowed in username")]
        public string UserName { get; set; }
        public string BussinessId { get; set; }
        public string RoleName { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required]
        [Display(Name = "Role")]
        public long RoleId { get; set; }
        public int Status { get; set; }
        public string OldPassword { get; set; }
        public bool Online { get; set; }
        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM dd, yyyy}")]
        public DateTime CreatedDate { get; set; }

        public long CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
