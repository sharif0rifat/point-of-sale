﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.INFRASTRUCTURE.ReportEntity
{
    public class InvoicePrint
    {
        public long Id { get; set; }
        public string To { get; set; }
        public long PartyId { get; set; }
        public string Address { get; set; }
        public string PrintDate { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceNo { get; set; }
        public decimal SubTotal { get; set; } 
        public Nullable<decimal> ServiceCharge { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<decimal> Vat { get; set; }
        public decimal GrandTotal { get; set; }
        public long PropertyId { get; set; }
        public string Remarks { get; set; }
        public string PartyName { get; set; }
        public string PartyAddress { get; set; }
        public string ProcessBy { get; set; }
        public string ApprovedBy { get; set; }
        public string ReferenceNo { get; set; }
        public string Currency { get; set; }
        public string NumberWords { get; set; }
    }
}
