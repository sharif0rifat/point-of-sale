﻿
namespace ERP.INFRASTRUCTURE.ReportEntity
{
    public class InvoicePrintDetail
    {
        public long Id { get; set; }
        public long ItemId { get; set; }
        public string ItemName { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public string Warranty { get; set; }
        public int WarrantyMonth { get; set; }
        public string ProductSerial { get; set; }
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
        public long Gift { get; set; }
    }
}