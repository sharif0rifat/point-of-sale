﻿using System;
using System.Web.Mvc;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;

namespace ERP.WEB.Controllers
{
    public class TemplateController : Controller
    {
        // GET: Template
        private readonly ITemplateRepo _templateRepo;
        private readonly IPermissionRepo _permissionRepo;
        public TemplateController(ITemplateRepo templateRepo, IPermissionRepo permissionRepo)
        {
            _templateRepo = templateRepo;
            _permissionRepo = permissionRepo;
        }

        public ActionResult List()
        {
            var agent = (LogInInfo)Session["loginInfo"];
            ViewBag.Templates = _templateRepo.GetTemplates(agent.UserName, agent.ApplicationId);
            return View();
        }

        public ActionResult Register()
        {
            ViewBag.ApplicationList = DropDownListClass.GetApplications();
            return View();
        }
        [HttpPost]
        public JsonResult Register(PartialTemplate param)
        {
            var agent = (LogInInfo)Session["loginInfo"];
            param.CreatedBy = agent.UserId;
            param.CreatedDate = DateTime.Today;
            param.Status = 1;
            var response = _templateRepo.Register(param);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Modify(long paramId)
        {
            var agent = (LogInInfo)Session["loginInfo"];
            var dbResponse = _templateRepo.GetTemplate(paramId, agent.ApplicationId);
            if (dbResponse.MessageType != 1) return RedirectToAction("List");
            return View(dbResponse.ReturnValue);
        }
        [HttpPost]
        public ActionResult Modify(PartialTemplate param)
        {
            var agent = (LogInInfo)Session["loginInfo"];
            param.ModifiedBy = agent.UserId;
            param.ModifiedDate = DateTime.Today;
            var dbResponse = _templateRepo.Modify(param);
            if (dbResponse.MessageType == 1) return RedirectToAction("Modify", new { paramId = Convert.ToInt64(dbResponse.ReturnValue) });
            return View();
        }
        [HttpPost]
        public JsonResult GetPermissionList(long param)
        {
            var response = _permissionRepo.GetPremissions(param);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}