﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using ERP.WEB.Helper;

namespace ERP.WEB.Controllers
{
    public class PermissionController : Controller
    {
        // GET: Permission
        private readonly IPermissionRepo _permissionRepo;

        public PermissionController(IPermissionRepo permissionRepo)
        {
            _permissionRepo = permissionRepo;
        }
        //
        // GET: /Permission/
        public ActionResult List()
        {
            ViewBag.Permissions = _permissionRepo.GetPremissions();
            return View();
        }
        [HttpGet]
        public ActionResult Register(string param = "")
        {
            ViewBag.PermissionCode = param;
            ViewBag.ApplicationList = DropDownListClass.GetApplications();


            var sysList = ControllerActionHelper.GetAllControllerAndAction();
            var controllerList = sysList.Select(i => i.Controller).Distinct().ToList();
            ViewBag.ControllerList = controllerList;
            return View();
        }
        [HttpPost]
        public JsonResult Register(PartialPermission param)
        {
            string actions = string.Empty;
            var controllerActions =
                ControllerActionHelper.GetAllControllerAndAction()
                    .Where(i => i.Controller == param.ControllerName).Select(i=>i.Action).Distinct().ToList();
            foreach (var controllerAction in controllerActions)
            {
                if (actions.Equals(string.Empty))
                {
                    actions = controllerAction;
                }
                else
                {
                    actions = actions + "^" + controllerAction;
                }
            }
            param.ActionName = actions;
            var response = _permissionRepo.Register(param);
            

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult Modify()
        //{
        //    ViewBag.ApplicationList = DropDownListClass.GetApplications();
        //    return View();
        //}
        [HttpPost]
        public JsonResult GetPermissionForModify(string param)
        {
            var response = _permissionRepo.GetPermission(param);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}