﻿using System.Linq;
using System.Reflection;
using ERP.REPOSITORIES.Repositories;
using System.Web.Mvc;

namespace ERP.WEB.Controllers
{
    public class TruncateController : Controller
    {
        //
        // GET: /Truncate/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TruncateData() 
        {
            Assembly asm = Assembly.GetExecutingAssembly();

            var controllers= asm.GetTypes()
                .Where(type => typeof(Controller).IsAssignableFrom(type)).ToList(); //filter controllers
            var methods = controllers.SelectMany(type => type.GetMethods())
                .Where(method => method.IsPublic && !method.IsDefined(typeof(NonActionAttribute))).ToList();

            //TruncatManager tManager = new TruncatManager();
            //tManager.TruncateAll();
            return View();
        }
	}
}