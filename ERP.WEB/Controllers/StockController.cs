﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using ERP.REPOSITORIES.IRepositories;

namespace ERP.WEB.Controllers
{
    public class StockController : Controller
    {

        private readonly IStockManager _stockManager;

        public StockController(IStockManager stockManager)
        {
            _stockManager = stockManager;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CurrentStockList()
        {
            try
            {
                var currentStockList = _stockManager.GetCurrentStocks();
                return View(currentStockList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult GEtProductStock(long? productId = null)
        {
            var stockInfo = _stockManager.GetStockByProduct(productId??0);
            return Json(new { isSuccess = true, lastPaymentInfo = stockInfo });
        }
	}
}