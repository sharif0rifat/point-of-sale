﻿using System;
using System.Web.Mvc;
using ERP.REPOSITORIES.Repositories;

namespace ERP.WEB.Controllers
{
    public class HomeController : Controller
    {
        //private readonly ICurrencyManager _currencyManager;
        //public HomeController(ICurrencyManager currencyManager)
        //{
        //    _currencyManager = currencyManager;
        //}
        public ActionResult Index()
        {
            Session.Timeout=300;
            ApplicationRepo repo = new ApplicationRepo();
            var app = repo.GetApplication();
            var remainingDays = (app.ValidateDate.Date - DateTime.Today.Date).TotalDays;
            if (app.ValidateDate < DateTime.Today)
            {
                return View("LicenseExpired");
            }
            else if (remainingDays < 30)
            {
                ViewBag.warningMessage = "You have " + remainingDays + " Days left of your license expiration.";
            }
            return View();
        }

        public ActionResult AuthFailed()
        {
            return View();
        }
    }
}