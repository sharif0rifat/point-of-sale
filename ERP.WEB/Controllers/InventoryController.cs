﻿using System.IO;
using System.Web;
using System.Web.Mvc;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;

namespace ERP.WEB.Controllers
{
    public class InventoryController : Controller
    {

        public const string SaveProuctPartial = "Partial/_SaveProductSettings";

        #region Inventory Setup
        private readonly LogInInfo _logInInfo;
        private readonly IProductManager _productManager;
        private readonly IStockManager _stockManager;

        #endregion

        public InventoryController(IProductManager productManager, IStockManager stockManager, HttpContext httpContext)
        {
            _productManager = productManager;
            _stockManager = stockManager;
            var httpContext1 = httpContext;
            if (httpContext1.Request.RequestContext.HttpContext.Session != null)
                _logInInfo = httpContext1.Request.RequestContext.HttpContext.Session["loginInfo"] as LogInInfo;
            else
            {
                _logInInfo = null;
            }
        }

        #region Product
        public ActionResult ProductSettingsList()
        {
            ViewBag.ProductList = _productManager.GetProducts();
            return View();
        }
        [HttpGet]
        public ActionResult SaveProductSettings(long Id)
        {
            ViewBag.CategoryList = DropDownListClass.GetCategories();
            ViewBag.response = AutoResponse.SuccessMessage();
            if (Id>0)
            {
                var product = _productManager.GetProduct(Id);
                product = product ?? new PartialProduct {  CategoryId=1};
                return View(SaveProuctPartial, product);
            }
            return View(SaveProuctPartial, new PartialProduct { CategoryId = 1 });
        }
        [HttpPost]
        public ActionResult SaveProductSettings(PartialProduct param)
        {
            if (param.ProductIamge!=null)
            {
                var fileName = Path.GetFileName(param.ProductIamge.FileName);
                var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~\\Upload\\Images"), fileName);
                param.ProductIamge.SaveAs(path);
            }
            var response = _productManager.AddProduct(param, _logInInfo);
            if (response.MessageType == 1)
            {
                return RedirectToAction("ProductSettingsList");
            }
                
            ViewBag.response = response;
            return View(SaveProuctPartial, new PartialProduct { CategoryId = 1 });
        }
        [HttpPost]
        public JsonResult DeleteProductSettings(long id)
        {
            var response = _productManager.DeleteProduct(id,_logInInfo);
            return Json(response,JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult LoadAmountByQuantity(long modelId, long quantity)
        {
            var amount = _stockManager.LoadAmountByQuantity(modelId, quantity);
            return Json(new { isSuccess = true, Amount = amount });
        }
    }
}