﻿using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.INFRASTRUCTURE.Helper;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using ERPSales.Web.Helper;
using ERP.REPOSITORIES.Repositories;

namespace ERP.WEB.Controllers
{
    public class PurchaseController : Controller
    {
        #region Initial Objects
        private readonly LogInInfo _logInInfo;
        private readonly IInvoiceInfoManager _invoiceInfoManager;
        private readonly IPurchaseInvoiceManager _purchaseInvoiceInfoManager;
        private readonly IInvoiceDetailsManager _invoiceDetailsManager;
        private readonly IProductManager _productManager;
        #endregion

        public PurchaseController(HttpContext httpContext, IInvoiceInfoManager invoiceInfoManager, IInvoiceDetailsManager invoiceDetailsManager, IPurchaseInvoiceManager purchaseInvoiceInfoManager, IProductManager productManager)
        {
            _invoiceInfoManager = invoiceInfoManager;
            _invoiceDetailsManager = invoiceDetailsManager;
            _productManager = productManager;
            _invoiceInfoManager = invoiceInfoManager;
            _purchaseInvoiceInfoManager = purchaseInvoiceInfoManager;

            var httpContext1 = httpContext;
            if (httpContext1.Request.RequestContext.HttpContext.Session != null)
                _logInInfo = httpContext1.Request.RequestContext.HttpContext.Session["loginInfo"] as LogInInfo;
            else
            {
                _logInInfo = null;
            }
        }
        public ActionResult PurchaseEntryList()
        {
            var model = new PuchaseListSearchModel
            {
                Id = 0,
                FromDate = DateTime.Today.AddDays(-30).ToString("dd/MMMM/yyyy"),
                ToDate = DateTime.Today.AddDays(1).ToString("dd/MMMM/yyyy")
            };
            DateTime fromDate = Convert.ToDateTime(model.FromDate);
            DateTime toDate = Convert.ToDateTime(model.ToDate);
            var invoiceList = _invoiceInfoManager.GetInvoiceInfoList(PropertyConstants.StockIn, "", fromDate,
                toDate);
            ViewBag.BillList = invoiceList;
            return View(model);
        }
        public ActionResult SavePurchaseEntry(long billId = 0)
        {
            var manager = new ProductManager();
            ViewBag.ProductList = manager.GetProducts();
            ViewBag.Title = "Product Input";
            PartialInvoiceInfo partialInvoiceInfo;
            if (billId > 0)
            {
                partialInvoiceInfo = _invoiceInfoManager.GetInvoiceByInvoiceId(billId);
                partialInvoiceInfo.PropertyId = PropertyConstants.StockIn;
                var billdetail = _invoiceDetailsManager.GetInvoiceDetails(billId);
                var prList = GetUpdatedProducts(manager.GetProducts(), billdetail);
                ViewBag.ProductList = prList;
            }
            else
            {
                partialInvoiceInfo = new PartialInvoiceInfo
                {
                    InvoiceNo = _invoiceInfoManager.GetDateGeneratedInvoiceNo(PropertyConstants.StockIn),
                    InvoiceDate = DateTime.Today,
                    PropertyId = PropertyConstants.StockIn,
                    InvoiceDetailsList = new List<InvoiceDetailViewModel>()
                };
            }
            return View(partialInvoiceInfo);
        }
        public ActionResult StockOutEntryList()
        {
            var model = new PuchaseListSearchModel
            {
                Id = 0,
                FromDate = DateTime.Today.AddDays(-30).ToString("dd/MMMM/yyyy"),
                ToDate = DateTime.Today.AddDays(1).ToString("dd/MMMM/yyyy")
            };
            DateTime fromDate = Convert.ToDateTime(model.FromDate);
            DateTime toDate = Convert.ToDateTime(model.ToDate);
            var invoiceList = _invoiceInfoManager.GetInvoiceInfoList(PropertyConstants.StockOut, "", fromDate,
                toDate);
            ViewBag.BillList = invoiceList;
            return View("PurchaseEntryList",model);
        }
        public ActionResult SaveStockOutEntry(long billId = 0)
        {
            var manager = new ProductManager();
            ViewBag.Title = "Product Out";
            ViewBag.ProductList = manager.GetProducts();
            PartialInvoiceInfo partialInvoiceInfo;
            if (billId > 0)
            {
                partialInvoiceInfo = _invoiceInfoManager.GetInvoiceByInvoiceId(billId);
                partialInvoiceInfo.PropertyId = PropertyConstants.StockOut;
                var billdetail = _invoiceDetailsManager.GetInvoiceDetails(billId);
                var prList = GetUpdatedProducts(manager.GetProducts(), billdetail);
                ViewBag.ProductList = prList;
            }
            else
            {
                partialInvoiceInfo = new PartialInvoiceInfo
                {
                    InvoiceNo = _invoiceInfoManager.GetDateGeneratedInvoiceNo(PropertyConstants.StockOut),
                    InvoiceDate = DateTime.Today,
                    PropertyId = PropertyConstants.StockOut,
                    InvoiceDetailsList = new List<InvoiceDetailViewModel>()
                };
            }
            return View("SavePurchaseEntry", partialInvoiceInfo);
        }
        private dynamic GetUpdatedProducts(List<PartialProduct> productList, List<InvoiceDetailViewModel> billdetail)
        {
            foreach (var item in billdetail)
            {
                foreach (var product in productList)
                {
                    if (item.ProductId == product.Id)
                    {
                        product.Quantity = item.Quantity;
                        product.SellingPrice = item.SellingPrice;
                        product.TotalPrice = item.Quantity * item.SellingPrice;
                    }
                }
            }
            return productList;
        }

        [HttpPost]
        public JsonResult OnChangLoadProduct(long param)
        {
            ViewBag.InvoiceType = PropertyConstants.Purchase;
            var products = _productManager.GetProductsByCategory(param);
            return Json(products, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SavePurchaseEntry(PartialInvoiceInfo param, List<InvoiceDetailViewModel> InvoiceDetailsList)
        {
            if (param != null && InvoiceDetailsList.Count > 0)
            {
                param.InvoiceDetailsList = InvoiceDetailsList;
                param.InvoiceDate = DateTime.Now;
            }
            else
                return Json(AutoResponse.FailedMessageWithParam("Product List is not uploaded"));
            DbResponse result = default(DbResponse);
            if (param.Id > 0)  //Edit
            {
                decimal sTotal = 0;
                for (var i = 0; i < InvoiceDetailsList.Count; i++)
                {
                    var totalProductprice = InvoiceDetailsList[i].Quantity * InvoiceDetailsList[i].SellingPrice;
                    sTotal = sTotal + totalProductprice;
                }
                param.SubTotal = sTotal;
                result = _purchaseInvoiceInfoManager.EditPurchaseInvoice(param, _logInInfo);
            }

            else //Create New 
            {
                decimal sTotal = 0;
                for (var i = 0; i < InvoiceDetailsList.Count; i++)
                {
                    var totalProductprice = InvoiceDetailsList[i].Quantity * InvoiceDetailsList[i].SellingPrice;
                    sTotal = sTotal + totalProductprice;
                }

                param.SubTotal = sTotal;

                result = _purchaseInvoiceInfoManager.AddPurchaseInvoice(param, _logInInfo);
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult LoadProduct(string productId)
        {
            return string.IsNullOrEmpty(productId) ?
                Json("", JsonRequestBehavior.AllowGet) :
                Json(new { isSuccess = true, Product = _productManager.GetProduct(Convert.ToInt64(productId)) });
        }
        public ActionResult LoadInvoiceInfoDetail(long invoiceInfoId)
        {

            IList<InvoiceDetailViewModel> detailLis = _invoiceDetailsManager.GetInvoiceDetails(invoiceInfoId);
            ViewBag.GiftList = DropDownListClass.GetGiftProperty();

            var view = "";
            foreach (var invoiceDetailViewModel in detailLis)
            {

                view += PartialView("~/Views/Purchase/Partial/_PartialInvoiceInfoDetail.cshtml", invoiceDetailViewModel)
                    .RenderToString();
            }

            var result = Json(view, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [HttpPost]
        public ActionResult GetNewInvoiceInfoDetail(long? invoiceInfoId, long productId)
        {
            try
            {
                //=============
                PartialProduct product = _productManager.GetProduct(productId);
                var newInvoiceDetail = new InvoiceDetailViewModel
                {
                    InvoiceId = invoiceInfoId.GetValueOrDefault(),
                    Status = 1,
                    ProductId = productId,
                    ProductName = product.ProductName,
                    BuyingPrice = product.BuyingPrice,
                    Quantity = 1,
                    TotalPrice = (product.BuyingPrice),
                };
                return PartialView("~/Views/Purchase/Partial/_PartialInvoiceInfoDetail.cshtml", newInvoiceDetail);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public JsonResult GetNewInvoiceInfoDetailBySerial(long invoiceId, long productId, int waranty, string serial)
        {
            try
            {
                if (productId < 1)
                    throw new Exception("No Product Is selected");
                IList<InvoiceDetailViewModel> detailLis = Helper.SerialExtractor.SerialRead(invoiceId, productId, serial, waranty);
                ViewBag.GiftList = DropDownListClass.GetGiftProperty();

                var view = "";
                foreach (var invoiceDetailViewModel in detailLis)
                {

                    view += PartialView("~/Views/Purchase/Partial/_PartialInvoiceInfoDetail.cshtml", invoiceDetailViewModel)
                        .RenderToString();
                }
                var response = AutoResponse.SuccessMessageWithValue("Loaded Successfully", view);
                var result = Json(response, JsonRequestBehavior.AllowGet);
                result.MaxJsonLength = int.MaxValue;
                return result;
            }
            catch (Exception ex)
            {
                var msg = AutoResponse.FailedMessageWithParam(ex.Message);
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
        }
        //public JsonResult GetNewInvoiceInfoDetailByExcel(long? invoiceInfoId, long productId)
        //{
        //    try
        //    {
        //        if (productId < 1)
        //            throw new Exception("No Product Is selected");
        //        IList<InvoiceDetailViewModel> detailLis = Helper.ExcelReader.CSVReader(invoiceInfoId, productId, _productManager);
        //        ViewBag.GiftList = DropDownListClass.GetGiftProperty();

        //        var view = "";
        //        foreach (var invoiceDetailViewModel in detailLis)
        //        {

        //            view += PartialView("~/Views/Purchase/Partial/_PartialInvoiceInfoDetail.cshtml", invoiceDetailViewModel)
        //                .RenderToString();
        //        }
        //        var response = AutoResponse.SuccessMessageWithValue("Loaded Successfully", view);
        //        var result = Json(response, JsonRequestBehavior.AllowGet);
        //        result.MaxJsonLength = int.MaxValue;
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        var msg = AutoResponse.FailedMessageWithParam(ex.Message);
        //        return Json(msg, JsonRequestBehavior.AllowGet);
        //    }

        //}
        [HttpPost]
        public ActionResult GetProductListByCategory(long categoryId)
        {
            ViewBag.ProductList = DropDownListClass.GetProductListByCategoryAndType(categoryId);
            if (ViewBag.ProductList.Items.Count < 2)
                return null;
            return PartialView("~/Views/Shared/Partial/_ProductsOptionPartial.cshtml");
        }
        [HttpPost]
        public JsonResult DeleteInvoiceInfo(string id)
        {
            long invoiceId;
            long.TryParse(id, out invoiceId);
            var result = new DbResponse();
            if (invoiceId > 0)
            {
                result = _invoiceInfoManager.DeleteInvoiceInfo(invoiceId, _logInInfo);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}