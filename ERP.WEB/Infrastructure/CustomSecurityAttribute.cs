﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;

namespace ERP.WEB.Infrastructure
{
    public class CustomSecurityAttribute : AuthorizeAttribute
    {
        private const string Waver = "localhost";
        public static bool GlobalFlag = false;
        public static string XmlFile = "";
        
        public CustomSecurityAttribute()
        {
            
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var controller = httpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            var action = httpContext.Request.RequestContext.RouteData.Values["action"].ToString();
            //if (controller == "AccountLogin") return true;
            if (action.ToLower() == "login" && controller.ToLower() == "account") return true;
            if (action.ToLower() == "logout" && controller.ToLower() == "account") return true;
            if (action.ToLower() == "changepassword" && controller.ToLower() == "account") return true;
            if (action.ToLower() == "passwordresetrequest" && controller.ToLower() == "account") return true;
            if (action.ToLower() == "accessdenied" && controller.ToLower() == "home") return true;

            if (httpContext.Request.RequestContext.HttpContext.Session == null ||
                httpContext.Request.RequestContext.HttpContext.Session["loginInfo"] == null) return false;
            var agent = (LogInInfo)httpContext.Request.RequestContext.HttpContext.Session["loginInfo"];
            //SaveLog(agent, controller, action);
            return agent.UserName == ConstantValue.Root || Authorize(agent, controller, action);
        }

        private static void GetUserIpAddress()
        {
            if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
            }
            else if (!string.IsNullOrEmpty(HttpContext.Current.Request.UserHostAddress))
            {
            }
        }
        private bool Authorize(LogInInfo logInInfo, string controller, string action)
        {
            var breakRequired = false;
            foreach (var partialPermission in logInInfo.Permissions)
            {
                if (breakRequired) break;
                if (partialPermission.ActionName.IndexOf('^') < 1)
                    breakRequired = logInInfo.Permissions.Any(i => i.ControllerName == controller && i.ActionName == action);
                var actionList = partialPermission.ActionName.Split('^');
                if (partialPermission.ControllerName == controller && actionList.Contains(action))
                {
                    breakRequired = true;
                }
            }
            return breakRequired;
        }
        
        protected override void HandleUnauthorizedRequest(AuthorizationContext context)
        {
            if (context.ActionDescriptor.ActionName.ToLower() == "logout" && context.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower() == "account")
            {
                // context.Result = new RedirectResult("~/Account/Login");
            }
            else if (context.ActionDescriptor.ActionName.ToLower() == "login" && context.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower() == "account")
            {
                //context.Result = new RedirectResult("~/Account/Login");
            }
            else if (context.ActionDescriptor.ActionName.ToLower() == "index" && context.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower() == "account")
            {
                //context.Result = new RedirectResult("~/Account/Login");
            }
            else if (context.ActionDescriptor.ActionName.ToLower() == "authfailed" && context.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower() == "home")
            {
                //context.Result = new RedirectResult("~/Account/Login");
            }
            else
            {
                if (context.RequestContext.HttpContext.Session == null) return;
                var logInInfo = (LogInInfo)context.RequestContext.HttpContext.Session["loginInfo"];
                context.Result = logInInfo == null ? new RedirectResult("~/Account/Login") : new RedirectResult("~/Home/AuthFailed");
            }
            //  base.HandleUnauthorizedRequest(context);
        }
    }
}