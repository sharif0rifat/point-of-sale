﻿using System.Web.Optimization;

namespace ERP.WEB
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*", "~/Scripts/jquery.validate.*", "~/Scripts/jquery.unobtrusive-ajax.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.

            bundles.Add(new ScriptBundle("~/bundles/sbadmin").Include(
                 "~/Scripts/jquery-ui.min.js",
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/jquery.mousewheel.js",
                      "~/Scripts/alertify.min.js",
                      "~/Scripts/plugins/jquery.inputmask.js",
                      "~/Scripts/plugins/metisMenu/metisMenu.min.js",
                      "~/Scripts/plugins/dataTables/jquery.dataTables.js",
                      "~/Scripts/plugins/dataTables/dataTables.bootstrap.js",
                      "~/Scripts/plugins/jqplot/jquery.jqplot.min.js",
                      "~/Scripts/plugins/jqplot/jqplot.barRenderer.js",
                      "~/Scripts/plugins/jqplot/jqplot.categoryAxisRenderer.js",
                      "~/Scripts/plugins/jqplot/jqplot.pointLabels.js",
                      "~/Scripts/underscore-min.js",
                      "~/Scripts/select2.min.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/customs").Include(
                "~/Scripts/Custom/autoLoad.js",
                "~/Scripts/Custom/Layout.js",
                "~/Scripts/Custom/InvoicePost.js",
                "~/Scripts/Custom/loadFromExcel.js",
                "~/Scripts/Custom/np-controller.js",
                "~/Scripts/Custom/popUpPartial.js",
                "~/Scripts/Custom/Report.js",
                "~/Scripts/Custom/SalesStatistics.js",
                "~/Scripts/Custom/submitter.js"
                ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/jquery-ui.css"
                      , "~/Content/css/bootstrap.min.css"
                      , "~/Content/css/plugins/timeline.css"
                      , "~/Content/css/plugins/alertify.core.css"
                      , "~/Content/css/plugins/alertify.default.css"
                      , "~/font-awesome-4.1.0/css/font-awesome.min.css"
                      , "~/Content/site.css"
                      , "~/Content/custom.css"
                      , "~/Content/css/select2.min.css"
                      ));
        }
    }
}
