﻿using AutoMapper;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;


namespace ERP.REPOSITORIES.Repositories
{
    public class PurchaseInvoiceManager : GenericRepository<ERPEntities, InvoiceInfo>, IPurchaseInvoiceManager
    {
        private readonly IInvoiceInfoManager _invoiceInfoManager;
        private readonly IInvoiceDetailsManager _invoiceDetailManager;
        private readonly IInvoiceDetailsManager _detailsManager;
        private readonly ICurrencyManager _currencyManager;

        public PurchaseInvoiceManager()
        {
            _invoiceInfoManager = new InvoiceInfoManager();
            _invoiceDetailManager = new InvoiceDetailsManager();
            _detailsManager = new InvoiceDetailsManager();
        }
        public List<PartialInvoiceInfo> GetPurchaseInvoiceInfoList(long invoiceType = 0, string invoiceNo = "", DateTime? fromDate = null, DateTime? toDate = null)
        {
            var startDate = fromDate ?? DateTime.Today.AddDays(-30);
            var endDate = toDate ?? DateTime.Today.AddDays(1);
            try
            {
                using (var db = new ERPEntities())
                {
                    var invoiceTypes = db.TableProperties.Where(i => i.PropertyName == "PurchaseInvoiceType").Select(i => i.Id);
                    var purchaseList = db.InvoiceInfoes.Where(invoiceInfo => invoiceInfo.Status == 1
                                                                             && invoiceInfo.InvoiceDate >= startDate &&
                                                                             invoiceInfo.InvoiceDate <= endDate && invoiceTypes.Contains(invoiceInfo.PropertyId))
                        .ToList();
                    if (!string.IsNullOrEmpty(invoiceNo))
                        purchaseList = purchaseList.Where(i => i.InvoiceNo == invoiceNo.Trim()).ToList();
                    else if (invoiceType > 0)
                        purchaseList = purchaseList.Where(i => i.PropertyId == invoiceType).ToList();
                    var result = Mapper.Map<List<PartialInvoiceInfo>>(purchaseList);
                    result.ForEach(i =>
                    {
                        i.PropertyName = db.TableProperties.FirstOrDefault(p => p.Id == i.PropertyId).ViewName;
                        i.CreatedByName = db.Users.FirstOrDefault(u => u.Id == i.CreatedBy).UserName;
                        i.PartyName = db.Ledgers.FirstOrDefault(pr => pr.Id == i.PartyId).Name;
                    });
                    return result;
                }

            }
            catch (Exception ex)
            {
                return new List<PartialInvoiceInfo>();
            }
        }
        public DbResponse AddPurchaseInvoice(PartialInvoiceInfo partialInvoiceInfo, LogInInfo logInInfo)
        {

            try
            {
                var invoiceInfo = Mapper.Map<InvoiceInfo>(partialInvoiceInfo);
                //===========
                invoiceInfo = _invoiceInfoManager.GetDebitCreditByInvoiceType(invoiceInfo); //Debit and Credit Header Id
                // TODO:
                invoiceInfo.ReferenceNo = invoiceInfo.ReferenceNo ?? "";
                //invoiceInfo.InvoiceNo = _invoiceInfoManager.GetDateGeneratedInvoiceNo(invoiceInfo.PropertyId);

                // Save To Invoice
                invoiceInfo = _invoiceInfoManager.AddInvoice(invoiceInfo, logInInfo);
                //====Save Detail to invoice=====
                if (partialInvoiceInfo.InvoiceDetailsList != null)
                {
                    List<InvoiceDetailViewModel> detailList = (List<InvoiceDetailViewModel>)partialInvoiceInfo.InvoiceDetailsList;
                    detailList.ForEach(i =>
                    {
                        i.PropertyId = invoiceInfo.PropertyId; 
                    });
                    _detailsManager.AddInvoiceDetailListToDB(detailList, invoiceInfo.Id, logInInfo);
                }
            }
            catch (Exception e)
            {
                return AutoResponse.FailedMessageWithParam(e);
            }

            return AutoResponse.SuccessMessageWithParam("Operation completed SuccessFully.");
        }
        public DbResponse EditPurchaseInvoice(PartialInvoiceInfo partialInvoiceInfo, LogInInfo logInInfo)
        {
            using (var dbEntities = new ERPEntities())
            {
                var invoiceInfo = dbEntities.InvoiceInfoes.Find(partialInvoiceInfo.Id);
                Mapper.Map(partialInvoiceInfo, invoiceInfo);

                partialInvoiceInfo.InvoiceDate = DateTime.Now;

                _invoiceInfoManager.UpdateInvoice(invoiceInfo, logInInfo);

                if (partialInvoiceInfo.InvoiceDetailsList == null) return AutoResponse.SuccessMessage();
                if (partialInvoiceInfo.InvoiceDetailsList == null) return AutoResponse.FailedMessageWithParam("No Products Added to update.");

                //Delete DetailList if removed or new added
                var dbDetailList = dbEntities.InvoiceDetails.Where(i => i.InvoiceId == partialInvoiceInfo.Id).ToList();
                if (partialInvoiceInfo.InvoiceDetailsList.Count != dbDetailList.Count)
                    return DeleteAndAddDetails(partialInvoiceInfo.InvoiceDetailsList, invoiceInfo, logInInfo);
                //Check two Lists Are Same
                var detailsId = partialInvoiceInfo.InvoiceDetailsList.Select(i => i.Id).ToList();
                var DBdetailsId = dbDetailList.Select(i => i.Id).ToList();
                if (DBdetailsId.Except(detailsId).Any())
                    return DeleteAndAddDetails(partialInvoiceInfo.InvoiceDetailsList, invoiceInfo, logInInfo);
                //=========Update Details======
                var detailList = Mapper.Map<List<InvoiceDetail>>(partialInvoiceInfo.InvoiceDetailsList);
                detailList.ForEach(i =>
                {
                    i.TotalPrice = i.BuyingPrice * i.Quantity;

                });
                foreach (var partialInvoiceDetail in partialInvoiceInfo.InvoiceDetailsList)
                {
                    var invoiceDetail = partialInvoiceDetail.Id > 0 ?
                         _invoiceDetailManager.GetInvoiceDetailsById(partialInvoiceDetail.Id) :
                         new InvoiceDetail();

                    invoiceDetail.CreatedDate = invoiceInfo.CreatedDate;
                    invoiceDetail.ModifiedDate = invoiceInfo.ModifiedDate;
                    invoiceDetail.CreatedBy = invoiceInfo.CreatedBy;
                    invoiceDetail.ModifiedBy = invoiceInfo.ModifiedBy;
                    invoiceDetail.InvoiceId = invoiceInfo.Id;
                    invoiceDetail.Quantity = partialInvoiceDetail.Quantity;

                    invoiceDetail.TotalPrice = partialInvoiceDetail.Quantity * partialInvoiceDetail.BuyingPrice;
                    invoiceDetail.ProductSerial = partialInvoiceDetail.ProductSerial;
                    invoiceDetail.Warranty = partialInvoiceDetail.Warranty;

                    if (partialInvoiceDetail.Id > 0)
                    {
                        // Update Invoice Details
                        _invoiceDetailManager.UpdateInvoiceDetails(invoiceDetail);
                    }
                    else
                    {
                        // Add Invoice Details
                        _invoiceDetailManager.AddInvoiceDetails(invoiceDetail);
                    }

                }
                _detailsManager.ChangeStockFlag(partialInvoiceInfo.InvoiceDetailsList, partialInvoiceInfo.PropertyId);
            }

            return AutoResponse.SuccessMessage();
        }

        private DbResponse DeleteAndAddDetails(IList<InvoiceDetailViewModel> InvoiceDetailsList, InvoiceInfo invoiceInfo, LogInInfo logInInfo)
        {
            try
            {
                _invoiceDetailManager.DeleteDetailByInvoiceId(invoiceInfo.Id);
                //var currencyInfo = _currencyManager.GetCurrency((int)invoiceInfo.Currency);
                if (InvoiceDetailsList != null)
                {
                    List<InvoiceDetailViewModel> detailList = (List<InvoiceDetailViewModel>)InvoiceDetailsList;
                    detailList.ForEach(i =>
                    {
                        i.TotalPrice = i.BuyingPrice * i.Quantity;
                        i.PropertyId = invoiceInfo.PropertyId;

                    });
                    _detailsManager.AddInvoiceDetailListToDB(detailList, invoiceInfo.Id, logInInfo);
                    _detailsManager.ChangeStockFlag(detailList, invoiceInfo.PropertyId);
                }
                return AutoResponse.SuccessMessage();
            }
            catch (Exception ex)
            {
                return AutoResponse.FailedMessageWithParam(ex.Message);
            }
        }
    }
}
