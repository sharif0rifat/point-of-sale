﻿using System;
using System.Collections.Generic;
using System.Linq;
using ERP.DAL.Model;
using ERP.REPOSITORIES.Generics;

namespace ERP.REPOSITORIES.Repositories
{
    public  class PropertyManager : GenericRepository<ERPEntities, TableProperty>, IPropertyManager
    {

        public TableProperty GetPropertyByNameAndValue(string name, Int64 value)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbProperty = db.TableProperties.FirstOrDefault(i => i.PropertyName == name && i.PropertyValue == value);
                    return dbProperty;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<TableProperty> GetPropertiesByName(string name)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbparties = db.TableProperties.Where(i => i.PropertyName == name).ToList();
                    return dbparties;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
