﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE.Entity;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;

namespace ERP.REPOSITORIES.Repositories
{
    public class CategoryManager : GenericRepository<ERPEntities, Category>, ICategoryManager
    {
        public List<PartialCategory> GetCategories()
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbCategoryList = db.Categories.Where(x => x.Status == 1).Select(i => i).OrderBy(i => i.Name).ToList();
                    return Mapper.Map<List<PartialCategory>>(dbCategoryList);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public PartialCategory GetCategory(long id)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbCategory = db.Categories.SingleOrDefault(x => x.Id == id);
                    var category = Mapper.Map<PartialCategory>(dbCategory);
                    return category;
                }
            }
            catch (Exception)
            {
                return new PartialCategory();
            }
        }
        public DbResponse AddCategory(PartialCategory categoryObj)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    
                    var checkCategory = db.Categories.Any(i => i.Name == categoryObj.Name && i.Status == 1 && i.Id != categoryObj.Id);
                    if (checkCategory) return AutoResponse.ExistMessage();
                    if (categoryObj.Id > 0)
                    {
                        var dbCategory = db.Categories.FirstOrDefault(i => i.Id == categoryObj.Id);
                        if (dbCategory == null) return AutoResponse.FailedMessage();
                        dbCategory.Name = categoryObj.Name;
                        dbCategory.Description = categoryObj.Description;
                        dbCategory.ModifiedBy = categoryObj.ModifiedBy;
                        dbCategory.ModifiedDate = categoryObj.ModifiedDate;
                        db.Entry(dbCategory).State = EntityState.Modified;
                    }
                    else
                    {
                        var dbCategory = Mapper.Map<Category>(categoryObj);
                        dbCategory.ModifiedDate = dbCategory.ModifiedDate=DateTime.Today;
                        dbCategory.Status = 1;
                        db.Categories.Add(dbCategory);
                    }                    
                    db.SaveChanges();
                    return AutoResponse.SuccessMessage();
                }
            }
            catch (Exception e)
            {
                return AutoResponse.FailedMessageWithParam(e);
               
            }

        }
        //public DbResponse EditCategory(PartialCategory categoryObj, LogInInfo info)
        //{
        //    try
        //    {
        //        using (var db = new ERPEntities())
        //        {
        //            var dbCategory = db.Categories.FirstOrDefault(i => i.Id == categoryObj.Id);
        //            if (dbCategory == null) return AutoResponse.NotFoundMessage();
        //            var checkCategory =
        //                db.Categories.Any(
        //                    a => a.Name == dbCategory.Name && a.Id != dbCategory.Id && a.Status == 1);
        //            if (checkCategory) return AutoResponse.ExistMessage();
        //            dbCategory.Name = categoryObj.Name;
        //            dbCategory.Description = categoryObj.Description;
        //            dbCategory.ModifiedBy = info.UserId;
        //            dbCategory.ModifiedDate = DateTime.Today;
        //            Edit(dbCategory);
        //            Save();
        //            return AutoResponse.SuccessMessage();
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        return AutoResponse.FailedMessageWithParam(exception);
        //    }
        //}
        public DbResponse DeleteCategory(long categoryId,LogInInfo logInInfo)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var isAny = db.Products.Where(i => i.CategoryId == categoryId && i.Status == 1).ToList();
                    if (isAny.Count > 0) return AutoResponse.FailedMessage("Cannot Delete Category...Some Product is udnder this category.");
                    var dbCategory = db.Categories.FirstOrDefault(i => i.Id == categoryId);
                    if (dbCategory == null) return AutoResponse.NotFoundMessage();
                    dbCategory.Status = 0;
                    dbCategory.ModifiedBy = logInInfo.UserId;
                    dbCategory.ModifiedDate = DateTime.Today;
                    Edit(dbCategory);
                    Save();
                    return AutoResponse.SuccessMessageWithParam("Category Deleted.");
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }
    }
}
