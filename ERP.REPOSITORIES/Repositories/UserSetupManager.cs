﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Entity;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using ERP.REPOSITORIES.System256;
using User = ERP.INFRASTRUCTURE.User;
using LogInInfo = ERP.INFRASTRUCTURE.Global.LogInInfo;
using UserLog = ERP.DAL.Model.UserLog;

namespace ERP.REPOSITORIES.Repositories
{
    public class UserSetupManager : GenericRepository<ERPEntities,DAL.Model.User>//,IUserSetupManager
    {
        //public DbResponse AddUser(User user,ERPEntities dbEntities)
        //{
        //    try
        //    {
        //        var checkUser = dbEntities.Users.Any(a => a.UserName == user.UserName);
        //        if (checkUser) return AutoResponse.ExistMessage();
        //        var dbUser = ConvertUserToDbUser(user, new DAL.Model.User());

        //        dbEntities.Users.Add(dbUser);
        //        dbEntities.SaveChanges();
        //        return AutoResponse.SuccessMessage();
        //    }
        //    catch (Exception e)
        //    {
        //        return AutoResponse.FailedMessageWithParam(e);
        //    }
        //}

        //public User ConvertDbUserToUser(DAL.Model.User dbUser, User user)
        //{
        //    var userConvert = new ConvertTypeInto<DAL.Model.User, User>();
        //    return userConvert.ConvertInto(dbUser, user);
        //}
        //public DAL.Model.User ConvertUserToDbUser(User user, DAL.Model.User dbUser)
        //{
        //    var userConvert = new ConvertTypeInto<User, DAL.Model.User>();
        //    return userConvert.ConvertInto(user, dbUser);
        //}

        //public DbResponse EditUser(User user,ERPEntities dbEntities)
        //{
        //    try
        //    {
        //        var dbUser = dbEntities.Users.FirstOrDefault(i => i.Id == user.Id);
        //        if (dbUser == null) return AutoResponse.NotFoundMessage();
        //        var checkUser = dbEntities.Users.Any(a => a.UserName == user.UserName && a.Id != user.Id  && a.RoleId == user.RoleId);
        //        if (checkUser) return AutoResponse.ExistMessage();
        //        dbUser = ConvertUserToDbUser(user, dbUser);
        //        dbUser.UserName = dbUser.UserName.Trim();
        //        Edit(dbUser);Save();
        //        return AutoResponse.SuccessMessage();
        //    }
        //    catch (Exception exception)
        //    {
        //        return AutoResponse.FailedMessageWithParam(exception);
        //    }
        //}

        //public DbResponse DeleteUser(long userid, int status, ERPEntities dbEntities)
        //{
        //    try
        //    {
        //        if (userid == 1) return AutoResponse.FailedMessage();
        //        var dbUser = dbEntities.Users.FirstOrDefault(i => i.Id == userid);
        //        if (dbUser == null) return AutoResponse.NotFoundMessage();
        //        dbUser.Status = status;
        //        Edit(dbUser);Save();
        //        return AutoResponse.SuccessMessage();
        //    }
        //    catch (Exception exception)
        //    {
        //        return AutoResponse.FailedMessageWithParam(exception);
        //    }
        //}

        //public List<User> GetUsers(long superRoleId)
        //{
        //    try
        //    {
        //        using (var db = new ERPEntities())
        //        {
        //            var users = new List<User>();
        //            var dbuserlist = db.Users.Where(i => i.Id != 1).ToList();
        //            users.AddRange(dbuserlist.Select(dbuser => new User
        //            {
        //                Id = dbuser.Id,
        //                UserName = dbuser.UserName,
        //                RoleName = db.Roles.Where(i => i.Id == dbuser.RoleId).Select(i => i.RoleName).SingleOrDefault(),
        //                Password = dbuser.Password,
        //                Status = dbuser.Status,
        //                RoleId = dbuser.RoleId
        //            }));
        //            return users;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return new List<User>();
        //    }
        //}

        //public User GetUser(long userId,User user,ERPEntities dbEntities)
        //{
        //    var dbuser = dbEntities.Users.FirstOrDefault(i => i.Id == userId);
        //    if (dbuser == null) return null;
        //    var userConverter = new ConvertTypeInto<DAL.Model.User, User>();
        //    user = userConverter.ConvertInto(dbuser, user);
        //    user.ConfirmPassword = user.Password;
        //    user.RoleName = dbEntities.Roles.Where(i => i.Id == dbuser.RoleId).Select(i => i.RoleName).SingleOrDefault();
        //    return user;
        //}

        //public static void SetUserLog(LogInInfo logInInfo,ERPEntities dbEntities)
        //{
        //    var logInfo = new UserLog
        //    {
        //        UserId = logInInfo.UserId, 
        //        RoleId = logInInfo.RoleId,
        //        LoginDateTime = System128.GetCurrentDateTime(),
        //        IpAddress = logInInfo.IpAddress
        //    };
        //    dbEntities.UserLogs.Add(logInfo);
        //    dbEntities.SaveChanges();
        //}
        //public DbResponse Login(User user,string ipAddress,ERPEntities dbEntities)
        //{
        //    var visitorsIpAddr = "";
        //    if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        //    {
        //        visitorsIpAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        //    }
        //    else if (!string.IsNullOrEmpty(HttpContext.Current.Request.UserHostAddress))
        //    {
        //        visitorsIpAddr = HttpContext.Current.Request.UserHostAddress;
        //    }
            
        //        var session = HttpContext.Current.Session;
        //        session["loginInfo"] = null;
        //        var dbUser = dbEntities.Users.FirstOrDefault(i => i.UserName == user.UserName && i.Password == user.Password);
        //        if (dbUser == null) return AutoResponse.NotFoundMessage();
        //        if (dbUser.Status == 0) return AutoResponse.NotFoundMessage();
        //    var loginInfo = new LogInInfo();
        //        // for super admin and administrator
        //        if (dbUser.Id == 1)
        //        {
        //            loginInfo.UserId = dbUser.Id;
        //            loginInfo.UserName = user.UserName;
        //            loginInfo.RoleId = dbUser.RoleId;
        //            loginInfo.RoleName =
        //                dbEntities.Roles.Where(i => i.Id == dbUser.RoleId).Select(i => i.RoleName).SingleOrDefault();
        //            loginInfo.IpAddress = visitorsIpAddr;
                   
        //            session["loginInfo"] = loginInfo;
        //            SetUserLog(loginInfo,dbEntities);
        //            UpdateUserStatus(dbUser,dbEntities);
        //            return AutoResponse.SuccessMessage();
        //        }
        //       // super admin and administrator login process end here.
        //        var dbrolePermissions = dbEntities.RolePermissions.Where(i => i.RoleId == dbUser.RoleId).Select(i=>i.PermissionId).ToList();
        //    if (dbrolePermissions.Count == 0) return AutoResponse.FailedMessage();
        //        var dbPermissions = ConvertToLocalObjects(dbEntities.Permissions.Where(i=>dbrolePermissions.Contains(i.Id)).ToList(), new List<PartialPermission>());

        //    loginInfo.UserId = dbUser.Id;
        //    loginInfo.UserName = user.UserName;
        //    loginInfo.PartialPermissions = dbPermissions;
        //    loginInfo.IpAddress = visitorsIpAddr;
                
        //        session["loginInfo"] = loginInfo;
        //        SetUserLog(loginInfo,dbEntities);
        //        UpdateUserStatus(dbUser,dbEntities);
        //    return AutoResponse.SuccessMessage();
        //}

        //private List<PartialPermission> ConvertToLocalObjects(List<Permission> convertedFrom, List<PartialPermission> convertedTo)
        //{
        //    var converter = new ConvertTypeInto<Permission, PartialPermission>();
        //    return converter.ConvertInto(convertedFrom, convertedTo);
        //}
        //// db object will converted to local object
        //public PartialPermission ConvertToLocalObject(Permission convertedFrom, PartialPermission convertedTo)
        //{
        //    var converter = new ConvertTypeInto<Permission, PartialPermission>();
        //    return converter.ConvertInto(convertedFrom, convertedTo);
        //}
        //// local object will converted to db object
        //public Permission ConvertToEntityObject(PartialPermission convertedFrom, Permission convertedTo)
        //{
        //    var converter = new ConvertTypeInto<PartialPermission, Permission>();
        //    return converter.ConvertInto(convertedFrom, convertedTo);
        //}

        //private static void UpdateUserStatus(DAL.Model.User useritem , ERPEntities dbEntities)
        //{
        //    if (useritem == null) return;
        //    useritem.Online = true;
        //    dbEntities.Entry(useritem).State = EntityState.Modified;
        //    dbEntities.SaveChanges();
        //}
        

        //public DbResponse ChangePassword(User model,ERPEntities dbEntities)
        //{
        //    try
        //    {
                
        //        var oldUser = dbEntities.Users.FirstOrDefault(i => i.Id == model.Id);
        //        if (oldUser == null) return AutoResponse.NotFoundMessage();
        //        if(oldUser.Password != model.OldPassword) return new DbResponse{MessageData = "Old password not matched",MessageType = 3};
        //        oldUser.Password = model.Password;
        //        dbEntities.Entry(oldUser).State = EntityState.Modified;
        //        dbEntities.SaveChanges();
        //        return AutoResponse.SuccessMessage();
        //    }
        //    catch (Exception exception)
        //    {
        //        return AutoResponse.FailedMessageWithParam(exception.ToString());
        //    }
        //}
        //public void UpdateLogoutStatus(ERPEntities dbEntities)
        //{
        //    var activeUser = dbEntities.Users.FirstOrDefault(i => i.Id == 1);
        //    if (activeUser == null) return;
        //    activeUser.Online = false;
        //    dbEntities.Entry(activeUser).State = EntityState.Modified;
        //    dbEntities.SaveChanges();
        //}


        //public List<User> GetUsers(long profileId, long superRoleId, List<User> users, ERPEntities dbEntities)
        //{
        //    throw new NotImplementedException();
        //}
    }
}