﻿using System;
using System.Linq;
using ERP.DAL.Model;
using ERP.REPOSITORIES.System256;

namespace ERP.REPOSITORIES.Repositories
{
    public class TruncatManager
    {
        public void TruncateAll(ERPEntities dbEntities)
        {
            object result;
            result = dbEntities.Salaries.SqlQuery("truncate table dbo.Salary").DefaultIfEmpty();
        }

        public void TruncateAll()
        {
            try
            {
                ERPEntities dbEntities = new ERPEntities();
                
                object result;
                result = dbEntities.Database.ExecuteSqlCommand("Truncate Table dbo.InvoiceInfo");
                result = dbEntities.Database.ExecuteSqlCommand("Truncate Table dbo.InvoiceDetails");
                result = dbEntities.Database.ExecuteSqlCommand("Truncate Table dbo.CurrentStock");
                result = dbEntities.Database.ExecuteSqlCommand("Truncate Table dbo.Payment");
                result = dbEntities.Database.ExecuteSqlCommand("Truncate Table dbo.Salary");
                result = dbEntities.Database.ExecuteSqlCommand("Truncate Table dbo.[Transaction]");
                result = dbEntities.Database.ExecuteSqlCommand("Truncate Table dbo.Stock");
            }
            catch (Exception)
            {
                throw;
            }
                     
        }
    }
}
