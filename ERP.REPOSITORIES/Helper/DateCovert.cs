﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.REPOSITORIES.Helper
{
    public class DateCovert
    {
        public static DateTime FromatDate(DateTime? date)
        {
            if (date == null)
                return DateTime.Today;
            var dateString = ((DateTime)date).ToString("dd/MMM/yyyy");
            var convertedDate = Convert.ToDateTime(dateString);
            return convertedDate;
        }
    }
}
