﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ERP.REPOSITORIES.Helper
{
    /// <summary>
    ///T is the input
    /// and E is Output
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="E"></typeparam>
    public class ConvertTypeInto<T,TE> where TE : new()
    {
        public TE ConvertInto(T convertable, TE convertedInto)
        {
            try
            {
                var myType = convertable.GetType();
                var props = new List<PropertyInfo>(myType.GetProperties());

                foreach (var prop in props)
                {
                    var propValue = prop.GetValue(convertable, null);
                    var prInfo = convertedInto.GetType().GetProperty(prop.Name);
                    if (prInfo!=null)
                        prInfo.SetValue(convertedInto, propValue);
                    // Do something with propValue
                }
                return convertedInto;
            }
            catch (Exception)
            {
                throw new Exception("Some Problem Happened Converting Entity Framework.");
            }
        }

        public List<TE> ConvertInto(List<T> convertable, List<TE> convertedInto)
        {
            try
            {
                var myType = convertable.First().GetType();
                var props = new List<PropertyInfo>(myType.GetProperties());
                
                foreach (var convert in convertable)
                {
                    var convertedIntoType = new TE();
                    foreach (var prop in props)
                    {
                        var propValue = prop.GetValue(convert, null);
                        var prInfo = convertedIntoType.GetType().GetProperty(prop.Name);
                        if (prInfo != null)
                            prInfo.SetValue(convertedIntoType, propValue);
                    }
                    convertedInto.Add(convertedIntoType);
                }
                return convertedInto;
            }
            catch (Exception)
            {
                throw new Exception("Some Problem Happened Converting Entity Framework.");
            }
        }
    }
}
