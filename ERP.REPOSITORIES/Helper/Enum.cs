﻿namespace ERP.REPOSITORIES.Helper
{
    public class Enum
    {
       public enum TransactionType
        {
            Sale = 1,
            Return,
            Due
        };
        public enum PaymentType
        {
            Partial = 1,
            Full,
            Return,
            DuePayment
        };

        public enum UserStatus
        {
            Inactive = -1,
            Active = 1,
            Lock,
            LostPassword,
            Required
        }

        public enum ItemStockEntryType
        {
            Purchase = 1,
            Transfer
        };

        public enum Month
        {
            January=1,
            February,
            March,
            April,
            May,
            June,
            July,
            August,
            September,
            October,
            November,
            December
        }
    }
}
