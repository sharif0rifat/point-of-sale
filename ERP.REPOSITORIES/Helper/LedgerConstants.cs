﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.REPOSITORIES.Helper
{
    public static class  LedgerConstants
    {
        public const long SalesLedgerId = 1;
        public const long PurchaseLedgerId = 2;
        public const long CurrentStockLedgerId = 3;
        public const long BonusIncomeLedgerId = 4;
        public const long ProductExpireExpenseLedgerId = 5;
        public const long ProfitIncomeLedgerId = 6;
        public const long PurchaseDiscountLedgerId = 7;
        public const long SalesDiscountLedgerId = 8;
        public const long PurchaseVatLedgerId = 9;
        public const long SalesVatLedgerId = 10;
        public const long PurchaseServiceChargeLedgerId = 11;
        public const long SalesServiceChargeLedgerId = 12;
        
        //Group Ledgers
        public const long DebtorsGroupLedgerId = 2;
        public const long CreditorsGroupLedgerId = 1;
    }
}
