﻿using System.Collections;
using System.Linq;
using ERP.DAL.Model;
using System;
using System.Collections.Generic;
using ERP.INFRASTRUCTURE;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.Repositories;

namespace ERP.REPOSITORIES.ReportRepositories
{
    public class SalesReportService
    {
        private readonly ERPEntities _entities;
        public SalesReportService()
        {
            _entities = new ERPEntities();
        }
       

        internal IEnumerable GetInvoiceReport(long categoryId, long productId, long partyId, DateTime startDate, DateTime endDate)
        {
            try
            {
                //=========Get Invoice by date=========
                endDate=endDate.AddHours(23);
                endDate = endDate.AddMinutes(59);
                //startDate=startDate.AddHours(-23);
                var dateinvoiceIds =
                   _entities.InvoiceInfoes.Where(
                       iv => iv.InvoiceDate >= startDate && iv.InvoiceDate <= endDate && iv.Status == 1 && iv.PropertyId==PropertyConstants.Sales
                      ).Select(iv => iv.Id)
                       .ToList();

                //Get Invoice Details vy Invoice
                var invoiceDetail = _entities.InvoiceDetails.Where(
                    i => dateinvoiceIds.Contains(i.InvoiceId)).ToList();

                //======filter by conditions========
                if (productId > 0)
                {
                    invoiceDetail = invoiceDetail.Where(i => i.Status == 1 && i.ProductId == productId).ToList();
                }
                if (categoryId > 0)
                {
                    var catProductIds = _entities.Products.Where(p => p.CategoryId == categoryId && p.Status == 1)
                        .Select(p => p.Id).ToList();
                    invoiceDetail = invoiceDetail.Where(
                        i => i.Status == 1 && catProductIds.Contains(i.ProductId)).ToList();
                }
                if (partyId > 0)
                {
                    var partyInvoiceIds = _entities.InvoiceInfoes.Where(iv => iv.PartyId == partyId && iv.Status == 1)
                        .Select(iv => iv.Id).ToList();
                    invoiceDetail =
                        invoiceDetail.Where(
                            i => i.Status == 1 && partyInvoiceIds.Contains(i.InvoiceId)).ToList();
                }

                IList<dynamic> reportData = new List<dynamic>();
                foreach (var invoice in invoiceDetail)
                {
                    var productName = _entities.Products.FirstOrDefault(p => p.Id == invoice.ProductId).ProductName;
                    var date = (DateTime)_entities.InvoiceInfoes.FirstOrDefault(iv => iv.Id == invoice.InvoiceId).InvoiceDate;
                    var partyName = _entities.Ledgers.FirstOrDefault(
                        p => p.Id == _entities.InvoiceInfoes.FirstOrDefault(iv => iv.Id == invoice.InvoiceId).PartyId)
                        .Name;
                    decimal purchaseQty = 0;
                    decimal salesQty = 0;

                    if (invoice.PropertyId == PropertyConstants.Purchase)
                    {
                        purchaseQty = invoice.Quantity;
                    }
                    else if (invoice.PropertyId == PropertyConstants.Sales)
                    {
                        salesQty = invoice.Quantity;
                    }
                    reportData.Add(
                            new
                            {
                                ProductName = productName,
                                Date = date.Date,
                                PartyName = partyName,
                                PurchaseQty = purchaseQty,
                                SalesQty = salesQty,
                                Balance = salesQty*invoice.SellingPrice
                            }
                        );
                }

                return reportData;
            }
            catch (Exception ex)
            {

                throw;
            }

        }
    }

}
