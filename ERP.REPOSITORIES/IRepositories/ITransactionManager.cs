﻿using System;
using System.Collections.Generic;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Entity;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;

namespace ERP.REPOSITORIES.IRepositories
{
    public interface ITransactionManager : IGenericRepository<ERP.DAL.Model.Transaction>, IDisposable
    {
        DbResponse AddTransaction(TransactionViewModel obj);
        List<TransactionViewModel> GetTransactions();
        List<TransactionViewModel> GetTransactionsBySubHeader(long ledgerId);
        TransactionViewModel GetTransaction(long objId);
        DbResponse EditTransaction(TransactionViewModel obj);
        DbResponse DeleteTransaction(long id, ERPEntities dbEntities);
        List<TransactionViewModel> GetTransactionsByDate(DateTime startDate,DateTime endDate);
    }
}
