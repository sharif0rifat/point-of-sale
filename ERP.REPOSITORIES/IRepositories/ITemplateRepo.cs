﻿using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;

namespace ERP.REPOSITORIES.IRepositories
{
    public interface ITemplateRepo
    {
        // get all templates
        DbResponse GetTemplates();
        // get templates by param key and param value
        DbResponse GetTemplates(string root,long applicationId);
        DbResponse Register(PartialTemplate param);
        DbResponse GetTemplate(long paramId,long applicationId);
        DbResponse Modify(PartialTemplate param);
    }
}
