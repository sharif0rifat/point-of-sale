﻿using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using System;
using System.Collections.Generic;

namespace ERP.REPOSITORIES.IRepositories
{
    public interface ISalesInvoiceManager
    {
        DbResponse AddSalesInvoice(PartialInvoiceInfo partialInvoiceInfo, LogInInfo logInInfo);
        DbResponse EditSalesInvoice(PartialInvoiceInfo partialInvoiceInfo, LogInInfo logInInfo);
        List<PartialInvoiceInfo> GetSalesInvoiceInfoList(long invoiceType = 0, string invoiceNo = "", DateTime? fromDate = default(DateTime?), DateTime? toDate = default(DateTime?));
        int GetWarantyBySerial(string serialNo);

        DbResponse DeleteInvoice(long invoiceId, LogInInfo _logInInfo);
    }
}