﻿using System.Collections.Generic;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using Infrastructure.ModelHelpers;

namespace ERP.REPOSITORIES.IRepositories
{
    public interface IPermissionRepo
    {
        // get all Premissions
        DbResponse GetPremissions();
        // get Premissions by param key and param value
        DbResponse GetPremissions(List<Searchy> searchies);
        DbResponse Register(PartialPermission permission);
        DbResponse GetPremissions(long applicationId);
        DbResponse GetPermission(string permissionCode);
        List<PartialPermission> ConvertToLocalObjects(List<Permission> convertedFrom,
            List<PartialPermission> convertedTo);
    }
}
