﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using EntityLayer.Models;
using Infrastructure.CoreModel;
using Infrastructure.ModelHelpers;
using Repository.Helpers;
using Repository.RepoInterfaces;

namespace Repository.Repos
{
    public class AuthRepo : IAuthRepo
    {
        public DbResponse Login(string param1, string param2, long param3)
        {
            try
            {
                var visitorsIpAddr = GetIpAddress();
                using (var db = new TerminalEntities())
                {
                    var session = HttpContext.Current.Session;
                    if (!(param1 != ConstantValue.Root || param2 != ConstantValue.RootPass))
                    {
                        var root = new Agent
                        {
                            UserId = 0,
                            UserName = ConstantValue.Root,
                            GroupId = 0,
                            GroupName = ConstantValue.Root,
                            GroupCode = ConstantValue.Root,
                            TemplateId = 0,
                            Islogin = true,
                            IpAddress = visitorsIpAddr,
                            Permissions = new List<PartialPermission> { new PartialPermission { Title = "All", PermissionCode = "ALL" } },
                            ApplicationId = 0,
                            ApplicationName = ConstantValue.Root
                        };
                        session["agent"] = root;
                        return AutoResponse.SuccessMessageWithParam(root);
                    }
                    if (param1 == ConstantValue.Root) param3 = 0;
                    var userFound = db.Users.FirstOrDefault(i => i.UserName == param1 && i.Password == param2 && i.ApplicationId == param3);
                    if (userFound == null) return AutoResponse.NotFoundMessage();
                    //if (application.ValidateDate > DateTime.Now.AddYears(100)) return AutoResponse.ExpiredMessage();
                    //if (userFound.Islogin || !userFound.IsActive) return AutoResponse.LoginExistMessage();
                    var permissions = db.Permissions.ToList();
                    var dbTemplate = db.Templates.FirstOrDefault(i => i.Id == userFound.TemplateId && i.Status==1);
                    if (dbTemplate == null) return AutoResponse.NotFoundMessage();
                    var dbGroup = db.Groups.FirstOrDefault(i => i.Id == userFound.GroupId && i.Status==1);
                    if (dbGroup == null) return AutoResponse.NotFoundMessage();
                    var dbApplication = db.Applications.FirstOrDefault(i => i.Id == userFound.ApplicationId && i.Status);
                    if (dbApplication == null) return AutoResponse.NotFoundMessage();
                    var agent = new Agent
                    {
                        UserId = userFound.Id,
                        UserName = userFound.UserName,
                        GroupId = userFound.GroupId,
                        GroupName = dbGroup.GroupName,
                        GroupCode = dbGroup.GroupCode,
                        TemplateId = userFound.TemplateId,
                        Islogin = userFound.Islogin,
                        IpAddress = visitorsIpAddr,
                        Permissions = PreparePermissionList(dbTemplate.PermissionIds, permissions),
                        ApplicationId = dbApplication.Id,
                        ApplicationName = dbApplication.AppName
                    };
                    userFound.Islogin = true;
                    userFound.LastLogin = DateTime.Now;
                    db.Entry(userFound).State = EntityState.Modified;
                    db.SaveChanges();
                    session["agent"] = null;
                    session["agent"] = agent;
                    return AutoResponse.SuccessMessageWithParam(agent);
                }
            }
            catch (Exception)
            {
                return AutoResponse.FailedMessage();
            }
        }

        private static string GetIpAddress()
        {
            //if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            //{
            //    return HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            //}
            //if (!string.IsNullOrEmpty(HttpContext.Current.Request.UserHostAddress))
            //{
                return HttpContext.Current.Request.UserHostAddress;
            //}
        }

        

        public DbResponse Logout(long param)
        {
            try
            {
                using (var db = new TerminalEntities())
                {
                    if (param == 0) return AutoResponse.SuccessMessageWithParam(param);
                    var loggedUser = db.Users.FirstOrDefault(i => i.Id == param);
                    if (loggedUser == null) return AutoResponse.NotFoundMessage();
                    loggedUser.Islogin = false;
                    db.Entry(loggedUser).State = EntityState.Modified;
                    db.SaveChanges();
                    return AutoResponse.SuccessMessageWithParam(param);
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        public DbResponse ResetPasswordRequest(long param)
        {
            try
            {
                using (var db = new TerminalEntities())
                {
                    var dbUser = db.Users.FirstOrDefault(i => i.Id == param);
                    if (dbUser == null) return AutoResponse.NotFoundMessage();
                    dbUser.Status = Convert.ToInt32(AutoSequence.UserStatus.LostPassword);
                    dbUser.UserMessage = "Password Lost";
                    db.Entry(dbUser).State = EntityState.Modified;
                    db.SaveChanges();
                    return AutoResponse.SuccessMessageWithParam(param);
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception.ToString());
            }
        }

        private List<PartialPermission> PreparePermissionList(string permissionIds, IEnumerable<Permission> permissions)
        {
            var permissionArr = permissionIds.Split('^').Select(Int64.Parse).ToList();
            var listed = permissions.Where(i => permissionArr.Contains(i.Id)).ToList();
            return ConvertToLocalObjects(listed, new List<PartialPermission>());
        }

        private List<PartialPermission> ConvertToLocalObjects(List<Permission> convertedFrom, List<PartialPermission> convertedTo)
        {
            var converter = new ConvertTypeInto<Permission, PartialPermission>();
            return converter.ConvertInto(convertedFrom, convertedTo);
        }
    }
}
