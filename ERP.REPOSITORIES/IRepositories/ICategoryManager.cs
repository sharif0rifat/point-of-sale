﻿using System;
using System.Collections.Generic;
using ERP.INFRASTRUCTURE.Entity;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;


namespace ERP.REPOSITORIES.IRepositories

{
    public interface ICategoryManager : IGenericRepository<DAL.Model.Category>, IDisposable
    {

        List<PartialCategory> GetCategories();
        DbResponse DeleteCategory(long categoryId, LogInInfo logInInfo);
        //DbResponse EditCategory(PartialCategory categoryObj, LogInInfo logInInfo);
        DbResponse AddCategory(PartialCategory categoryObj);
        PartialCategory GetCategory(long id);

    }
}
