﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using User = ERP.INFRASTRUCTURE.User;

namespace ERP.REPOSITORIES.IRepositories
{
    public interface IUserSetupManager : IGenericRepository<ERP.DAL.Model.User>,IDisposable
    {
        DbResponse AddUser(User user, ERPEntities dbEntities);
        DbResponse EditUser(User user, ERPEntities dbEntities);
        DbResponse DeleteUser(long userid, int status, ERPEntities dbEntities);
        User GetUser(long userId, User user, ERPEntities dbEntities);
        DbResponse Login(User user, string ipAddress, ERPEntities dbEntities);
        List<User> GetUsers(long superRoleId);
        DbResponse ChangePassword(User model, ERPEntities dbEntities);
        void UpdateLogoutStatus(ERPEntities dbEntities);
    }
}
