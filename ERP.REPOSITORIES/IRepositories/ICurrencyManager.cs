﻿using ERP.INFRASTRUCTURE.Entity;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.REPOSITORIES.IRepositories
{
    public interface ICurrencyManager : IGenericRepository<DAL.Model.Currency>, IDisposable
    {
        List<PartialCurrency> GetCurrencies();
        PartialCurrency GetCurrency(int id);
        PartialCurrency GetCurrencyByCountry(string country);
        PartialCurrency GetCurrencyBySymbol(string symbol);

        DbResponse AddCurrency(PartialCurrency currencyObj);

        DbResponse DeleteCurrency(long currencyId, LogInInfo logInInfo);
    }
}