﻿using System;
using System.Collections.Generic;
using ERP.DAL.Model;
using ERP.REPOSITORIES.Generics;

namespace ERP.REPOSITORIES.Repositories
{
    public interface IPropertyManager : IGenericRepository<TableProperty>, IDisposable
    {
        TableProperty GetPropertyByNameAndValue(string name, Int64 vlaue);
        List<TableProperty> GetPropertiesByName(string name);
    }
}
