﻿using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using System;
using System.Collections.Generic;
using Payment = ERP.INFRASTRUCTURE.Payment;


namespace ERP.REPOSITORIES.IRepositories
{
    public interface IPaymentManager : IGenericRepository<DAL.Model.Payment>, IDisposable
    {
        decimal GetPaymentInfo(long partyId);
        decimal GetPaymentInfo(long partyId, DateTime bllingDate);
        List<Payment> GetPaymentInfoByStatus(long paymentType, long paymentFlag = 0);
        INFRASTRUCTURE.Payment GetLastPayment(long partyId);
        List<Payment> GetPayments(long? partyId = null);
        DbResponse AddPayment(Payment payemnt, LogInInfo _logInInfo);
        Payment GetPayment(long id);
        DbResponse EditPayment(Payment payemnt, LogInInfo logInInfo);
        DbResponse DeletePayment(long id, LogInInfo _logInInfo);
        List<Payment> GetPaymentsByDate(long? partyId, DateTime? datefrom, DateTime? datetto);
        List<Payment> GetPaymentsByDate(long? partyId, long paymentType, DateTime? datefrom, DateTime? datetto);
        Payment GetPaymentById(long id);

        List<Payment> GetPaymentsByType(long? partyId, long transactionType);
    }
}
