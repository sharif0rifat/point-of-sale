﻿using System;
using System.Collections.Generic;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using Salary = ERP.INFRASTRUCTURE.Salary;
namespace ERP.REPOSITORIES.IRepositories
{
    public interface ISalaryManager : IGenericRepository<DAL.Model.Salary>, IDisposable
    {
        List<Salary> GetSalaries();
        DbResponse DeleteSalary(long id, ERPEntities dbEntities);
        DbResponse EditSalary(Salary obj, ERPEntities dbEntities);
        DbResponse AddSalary(INFRASTRUCTURE.Salary obj, ERPEntities dbEntities);
        Salary GetSalary(long id, ERPEntities dbEntities);
        
    }
}
