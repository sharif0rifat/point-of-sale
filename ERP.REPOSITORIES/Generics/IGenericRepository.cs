﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace ERP.REPOSITORIES.Generics
{
    public interface IGenericRepository<T> : IDisposable where T : class
    {
        IQueryable<T> GetAll();
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        void Add(T entity);
        void AddToDb(T entity, String jSonStr);
        T AddToDb(T entity);
        void Delete(T entity);
        void Edit(T entity);
        void Save();
    }
}
