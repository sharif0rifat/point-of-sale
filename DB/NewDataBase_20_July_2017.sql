USE [RPOS]
GO
/****** Object:  Table [dbo].[Application]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Application](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AppName] [nvarchar](50) NOT NULL,
	[LogoUrl] [nvarchar](50) NULL,
	[Slogon] [nvarchar](max) NULL,
	[ValidateDate] [date] NOT NULL,
	[Status] [bit] NOT NULL CONSTRAINT [DF_Application_Status]  DEFAULT ((1)),
 CONSTRAINT [PK_Application] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Category]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[IsParent] [bit] NOT NULL,
	[ParentId] [bigint] NULL,
	[BrandName] [nvarchar](200) NULL,
 CONSTRAINT [PK_Brand] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Currency]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[CreatedDate] [date] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[ModifiedDate] [date] NOT NULL,
	[Status] [int] NOT NULL,
	[CurrencyName] [nvarchar](50) NOT NULL,
	[ShortName] [nvarchar](50) NOT NULL,
	[Symbol] [nvarchar](15) NOT NULL,
	[Country] [nvarchar](50) NOT NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[EmployeeName] [nvarchar](100) NULL,
	[EmployeeCode] [nvarchar](100) NULL,
	[Designation] [nvarchar](100) NULL,
	[Department] [nvarchar](100) NULL,
	[Location] [nvarchar](100) NULL,
	[JoiningDate] [datetime] NULL,
	[DateOfBirth] [datetime] NULL,
	[BasicSalary] [decimal](18, 0) NULL,
	[HouseRent] [decimal](18, 0) NULL,
	[MobileInternet] [decimal](18, 0) NULL,
	[Transport] [decimal](18, 0) NULL,
	[Wellbeing] [decimal](18, 0) NULL,
	[Others] [decimal](18, 0) NULL,
	[Advance] [decimal](18, 0) NULL,
	[LeaveDeduction] [decimal](18, 0) NULL,
	[LateDeduction] [decimal](18, 0) NULL,
	[MiscellaneousDeduction] [decimal](18, 0) NULL,
	[Tax] [decimal](18, 0) NULL,
	[ProvidentFund] [decimal](18, 0) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupHeader]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupHeader](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NULL,
	[AccountsType] [bigint] NULL,
	[IsParent] [bit] NOT NULL,
 CONSTRAINT [PK_GroupHeader] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceDetails]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceDetails](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[InvoiceId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[SellingPrice] [decimal](18, 2) NOT NULL,
	[Quantity] [decimal](18, 2) NOT NULL,
	[TotalPrice] [decimal](18, 2) NOT NULL,
	[PropertyId] [bigint] NOT NULL,
	[BuyingPrice] [decimal](18, 2) NOT NULL,
	[ProductSerial] [nvarchar](50) NOT NULL,
	[Warranty] [int] NOT NULL,
	[StockIn] [bigint] NOT NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Currency] [bigint] NOT NULL,
 CONSTRAINT [PK_BillDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceInfo]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceInfo](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[InvoiceNo] [nvarchar](100) NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[SubTotal] [decimal](18, 0) NOT NULL,
	[ServiceCharge] [decimal](18, 0) NULL,
	[Discount] [decimal](18, 2) NULL,
	[Vat] [decimal](18, 2) NULL,
	[GrandTotal] [decimal](18, 0) NOT NULL,
	[InvoiceReferenceId] [bigint] NULL,
	[PropertyId] [bigint] NOT NULL,
	[Remarks] [nvarchar](max) NULL,
	[InvoiceDate] [datetime] NULL,
	[DebitId] [bigint] NOT NULL,
	[CreditId] [bigint] NOT NULL,
	[PartyName] [nvarchar](500) NULL,
	[PartyAddress] [nvarchar](2000) NULL,
	[ProcessBy] [bigint] NOT NULL,
	[ApprovedBy] [bigint] NOT NULL,
	[ReferenceNo] [nvarchar](100) NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Currency] [bigint] NOT NULL,
 CONSTRAINT [PK_BillingTable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ledger]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ledger](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[LedgerGroupId] [bigint] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NOT NULL,
	[Code] [nvarchar](max) NULL,
	[ContactPerson] [nvarchar](max) NULL,
	[Address1] [nvarchar](max) NULL,
	[Address2] [nvarchar](max) NULL,
	[Mobile1] [nvarchar](max) NULL,
	[Mobile2] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[Visible] [bigint] NULL,
 CONSTRAINT [PK_Ledger] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LedgerGroup]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LedgerGroup](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NULL,
	[GroupHeaderId] [bigint] NOT NULL,
	[IsParent] [bit] NOT NULL CONSTRAINT [DF_TransactionHeader_IsParent]  DEFAULT ((0)),
 CONSTRAINT [PK_LedgerGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Payment]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [nvarchar](50) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[PaymentType] [bigint] NOT NULL,
	[Remarks] [nvarchar](200) NULL,
	[DebitId] [bigint] NOT NULL,
	[CreditId] [bigint] NOT NULL,
	[BankName] [nvarchar](100) NULL,
	[AccountNo] [nvarchar](100) NULL,
	[ReferenceNo] [nvarchar](100) NULL,
	[CheckNo] [nvarchar](100) NULL,
	[CheckDate] [date] NULL,
	[PaymentStatus] [bigint] NOT NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Currency] [bigint] NOT NULL,
 CONSTRAINT [PK_Payment_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Permission]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permission](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[PermissionCode] [nchar](10) NULL,
	[ControllerName] [nvarchar](20) NULL,
	[ActionName] [nvarchar](max) NULL,
	[Status] [bit] NULL,
	[ApplicationId] [bigint] NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[CategoryId] [bigint] NOT NULL,
	[ProductName] [nvarchar](100) NOT NULL,
	[BuyingPrice] [decimal](18, 2) NOT NULL,
	[Commission] [decimal](18, 2) NULL,
	[SellingPrice] [decimal](18, 2) NOT NULL,
	[ProductCode] [nvarchar](50) NULL,
	[ImageUrl] [nvarchar](max) NULL,
 CONSTRAINT [PK_Model] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Salary]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salary](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[EmployeeId] [bigint] NOT NULL,
	[SalryType] [bigint] NULL,
	[Month] [datetime] NULL,
	[TotalSalary] [decimal](18, 0) NULL,
	[BasicSalary] [decimal](18, 0) NULL,
	[HouseRent] [decimal](18, 0) NULL,
	[MobileAllowance] [decimal](18, 0) NULL,
	[TransportCost] [decimal](18, 0) NULL,
	[FestivalBonus] [decimal](18, 0) NULL,
	[TotalBill] [decimal](18, 0) NULL,
	[MedicalAllowance] [decimal](18, 0) NULL,
	[OtherAllowance] [decimal](18, 0) NULL,
	[OtherDeduction] [decimal](18, 0) NULL,
	[DebitId] [bigint] NOT NULL,
	[CreditId] [bigint] NOT NULL,
 CONSTRAINT [PK_Salary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TableProperty]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableProperty](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TableName] [nvarchar](50) NULL,
	[PropertyName] [nvarchar](50) NULL,
	[PropertyValue] [int] NULL,
	[ViewName] [nvarchar](100) NULL,
 CONSTRAINT [PK_TableProperty] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Template]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Template](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](50) NULL,
	[PermissionIds] [nvarchar](max) NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_Role_Status]  DEFAULT ((1)),
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [date] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [date] NULL,
	[ApplicationId] [bigint] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [nvarchar](50) NOT NULL,
	[DebitHeaderId] [bigint] NOT NULL,
	[CreditHeaderId] [bigint] NOT NULL,
	[Amount] [decimal](18, 2) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[TransactionType] [bigint] NOT NULL,
	[CheckNo] [nvarchar](50) NULL,
	[EmployeeId] [bigint] NULL,
	[Remarks] [nvarchar](max) NULL,
	[BankId] [bigint] NULL,
	[CashType] [int] NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Currency] [bigint] NOT NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Unit]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Unit](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Symbol] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Unit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_User_IsActive]  DEFAULT ((1)),
	[Islogin] [bit] NOT NULL CONSTRAINT [DF_User_Islogin]  DEFAULT ((0)),
	[LastLogin] [datetime] NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_User_Status]  DEFAULT ((1)),
	[PhotoUrl] [nvarchar](50) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[CreatedDate] [date] NOT NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [date] NULL,
	[TemplateId] [bigint] NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[ApplicationId] [bigint] NOT NULL,
	[UserMessage] [nvarchar](max) NULL,
	[Name] [nvarchar](200) NULL,
	[IsCreator] [bit] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserLog]    Script Date: 20-07-17 6:28:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NULL,
	[ProfileId] [bigint] NULL,
	[RoleId] [bigint] NULL,
	[LoginDateTime] [datetime] NULL,
	[IpAddress] [nvarchar](50) NULL,
 CONSTRAINT [PK_UserLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[InvoiceDetails] ADD  CONSTRAINT [DF_InvoiceDetails_ProductSerial]  DEFAULT ((0)) FOR [ProductSerial]
GO
ALTER TABLE [dbo].[InvoiceDetails] ADD  CONSTRAINT [DF_InvoiceDetails_SalesFlag]  DEFAULT ((0)) FOR [StockIn]
GO
