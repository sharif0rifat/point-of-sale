USE [master]
GO
/****** Object:  Database [RPOS]    Script Date: 16/08/17 12:29:49 PM ******/
CREATE DATABASE [RPOS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RPOS', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\RPOS.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'RPOS_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\RPOS_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [RPOS] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RPOS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RPOS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RPOS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RPOS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RPOS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RPOS] SET ARITHABORT OFF 
GO
ALTER DATABASE [RPOS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RPOS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RPOS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RPOS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RPOS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RPOS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RPOS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RPOS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RPOS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RPOS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [RPOS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RPOS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RPOS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RPOS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RPOS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RPOS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RPOS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RPOS] SET RECOVERY FULL 
GO
ALTER DATABASE [RPOS] SET  MULTI_USER 
GO
ALTER DATABASE [RPOS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RPOS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RPOS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RPOS] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [RPOS] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'RPOS', N'ON'
GO
USE [RPOS]
GO
/****** Object:  Table [dbo].[Application]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Application](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AppName] [nvarchar](50) NOT NULL,
	[LogoUrl] [nvarchar](50) NULL,
	[Slogon] [nvarchar](max) NULL,
	[ValidateDate] [date] NOT NULL,
	[Status] [bit] NOT NULL CONSTRAINT [DF_Application_Status]  DEFAULT ((1)),
 CONSTRAINT [PK_Application] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Category]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[IsParent] [bit] NOT NULL,
	[ParentId] [bigint] NULL,
	[BrandName] [nvarchar](200) NULL,
 CONSTRAINT [PK_Brand] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Currency]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[CreatedDate] [date] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[ModifiedDate] [date] NOT NULL,
	[Status] [int] NOT NULL,
	[CurrencyName] [nvarchar](50) NOT NULL,
	[ShortName] [nvarchar](50) NOT NULL,
	[Symbol] [nvarchar](15) NOT NULL,
	[Country] [nvarchar](50) NOT NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[EmployeeName] [nvarchar](100) NULL,
	[EmployeeCode] [nvarchar](100) NULL,
	[Designation] [nvarchar](100) NULL,
	[Department] [nvarchar](100) NULL,
	[Location] [nvarchar](100) NULL,
	[JoiningDate] [datetime] NULL,
	[DateOfBirth] [datetime] NULL,
	[BasicSalary] [decimal](18, 0) NULL,
	[HouseRent] [decimal](18, 0) NULL,
	[MobileInternet] [decimal](18, 0) NULL,
	[Transport] [decimal](18, 0) NULL,
	[Wellbeing] [decimal](18, 0) NULL,
	[Others] [decimal](18, 0) NULL,
	[Advance] [decimal](18, 0) NULL,
	[LeaveDeduction] [decimal](18, 0) NULL,
	[LateDeduction] [decimal](18, 0) NULL,
	[MiscellaneousDeduction] [decimal](18, 0) NULL,
	[Tax] [decimal](18, 0) NULL,
	[ProvidentFund] [decimal](18, 0) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupHeader]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupHeader](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NULL,
	[AccountsType] [bigint] NULL,
	[IsParent] [bit] NOT NULL,
 CONSTRAINT [PK_GroupHeader] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceDetails]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceDetails](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[InvoiceId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[SellingPrice] [decimal](18, 2) NOT NULL,
	[Quantity] [decimal](18, 2) NOT NULL,
	[TotalPrice] [decimal](18, 2) NOT NULL,
	[PropertyId] [bigint] NOT NULL,
	[BuyingPrice] [decimal](18, 2) NOT NULL,
	[ProductSerial] [nvarchar](50) NOT NULL CONSTRAINT [DF_InvoiceDetails_ProductSerial]  DEFAULT ((0)),
	[Warranty] [int] NOT NULL,
	[StockIn] [bigint] NOT NULL CONSTRAINT [DF_InvoiceDetails_SalesFlag]  DEFAULT ((0)),
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Currency] [bigint] NOT NULL,
 CONSTRAINT [PK_BillDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceInfo]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceInfo](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[InvoiceNo] [nvarchar](100) NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[SubTotal] [decimal](18, 0) NOT NULL,
	[ServiceCharge] [decimal](18, 0) NULL,
	[Discount] [decimal](18, 2) NULL,
	[Vat] [decimal](18, 2) NULL,
	[GrandTotal] [decimal](18, 0) NOT NULL,
	[InvoiceReferenceId] [bigint] NULL,
	[PropertyId] [bigint] NOT NULL,
	[Remarks] [nvarchar](max) NULL,
	[InvoiceDate] [datetime] NULL,
	[DebitId] [bigint] NOT NULL,
	[CreditId] [bigint] NOT NULL,
	[PartyName] [nvarchar](500) NULL,
	[PartyAddress] [nvarchar](2000) NULL,
	[ProcessBy] [bigint] NOT NULL,
	[ApprovedBy] [bigint] NOT NULL,
	[ReferenceNo] [nvarchar](100) NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Currency] [bigint] NOT NULL,
 CONSTRAINT [PK_BillingTable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ledger]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ledger](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[LedgerGroupId] [bigint] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NOT NULL,
	[Code] [nvarchar](max) NULL,
	[ContactPerson] [nvarchar](max) NULL,
	[Address1] [nvarchar](max) NULL,
	[Address2] [nvarchar](max) NULL,
	[Mobile1] [nvarchar](max) NULL,
	[Mobile2] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[Visible] [bigint] NULL,
 CONSTRAINT [PK_Ledger] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LedgerGroup]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LedgerGroup](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NULL,
	[GroupHeaderId] [bigint] NOT NULL,
	[IsParent] [bit] NOT NULL CONSTRAINT [DF_TransactionHeader_IsParent]  DEFAULT ((0)),
 CONSTRAINT [PK_LedgerGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Payment]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [nvarchar](50) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[PaymentType] [bigint] NOT NULL,
	[Remarks] [nvarchar](200) NULL,
	[DebitId] [bigint] NOT NULL,
	[CreditId] [bigint] NOT NULL,
	[BankName] [nvarchar](100) NULL,
	[AccountNo] [nvarchar](100) NULL,
	[ReferenceNo] [nvarchar](100) NULL,
	[CheckNo] [nvarchar](100) NULL,
	[CheckDate] [date] NULL,
	[PaymentStatus] [bigint] NOT NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Currency] [bigint] NOT NULL,
 CONSTRAINT [PK_Payment_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Permission]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permission](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[PermissionCode] [nchar](10) NULL,
	[ControllerName] [nvarchar](20) NULL,
	[ActionName] [nvarchar](max) NULL,
	[Status] [bit] NULL,
	[ApplicationId] [bigint] NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[CategoryId] [bigint] NOT NULL,
	[ProductName] [nvarchar](100) NOT NULL,
	[BuyingPrice] [decimal](18, 2) NOT NULL,
	[Commission] [decimal](18, 2) NULL,
	[SellingPrice] [decimal](18, 2) NOT NULL,
	[ProductCode] [nvarchar](50) NULL,
	[ImageUrl] [nvarchar](max) NULL,
 CONSTRAINT [PK_Model] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Salary]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salary](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[EmployeeId] [bigint] NOT NULL,
	[SalryType] [bigint] NULL,
	[Month] [datetime] NULL,
	[TotalSalary] [decimal](18, 0) NULL,
	[BasicSalary] [decimal](18, 0) NULL,
	[HouseRent] [decimal](18, 0) NULL,
	[MobileAllowance] [decimal](18, 0) NULL,
	[TransportCost] [decimal](18, 0) NULL,
	[FestivalBonus] [decimal](18, 0) NULL,
	[TotalBill] [decimal](18, 0) NULL,
	[MedicalAllowance] [decimal](18, 0) NULL,
	[OtherAllowance] [decimal](18, 0) NULL,
	[OtherDeduction] [decimal](18, 0) NULL,
	[DebitId] [bigint] NOT NULL,
	[CreditId] [bigint] NOT NULL,
 CONSTRAINT [PK_Salary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TableProperty]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableProperty](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TableName] [nvarchar](50) NULL,
	[PropertyName] [nvarchar](50) NULL,
	[PropertyValue] [int] NULL,
	[ViewName] [nvarchar](100) NULL,
 CONSTRAINT [PK_TableProperty] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Template]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Template](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](50) NULL,
	[PermissionIds] [nvarchar](max) NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_Role_Status]  DEFAULT ((1)),
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [date] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [date] NULL,
	[ApplicationId] [bigint] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [nvarchar](50) NOT NULL,
	[DebitHeaderId] [bigint] NOT NULL,
	[CreditHeaderId] [bigint] NOT NULL,
	[Amount] [decimal](18, 2) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[TransactionType] [bigint] NOT NULL,
	[CheckNo] [nvarchar](50) NULL,
	[EmployeeId] [bigint] NULL,
	[Remarks] [nvarchar](max) NULL,
	[BankId] [bigint] NULL,
	[CashType] [int] NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Currency] [bigint] NOT NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Unit]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Unit](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Symbol] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Unit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_User_IsActive]  DEFAULT ((1)),
	[Islogin] [bit] NOT NULL CONSTRAINT [DF_User_Islogin]  DEFAULT ((0)),
	[LastLogin] [datetime] NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_User_Status]  DEFAULT ((1)),
	[PhotoUrl] [nvarchar](50) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[CreatedDate] [date] NOT NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [date] NULL,
	[TemplateId] [bigint] NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[ApplicationId] [bigint] NOT NULL,
	[UserMessage] [nvarchar](max) NULL,
	[Name] [nvarchar](200) NULL,
	[IsCreator] [bit] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserLog]    Script Date: 16/08/17 12:29:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NULL,
	[ProfileId] [bigint] NULL,
	[RoleId] [bigint] NULL,
	[LoginDateTime] [datetime] NULL,
	[IpAddress] [nvarchar](50) NULL,
 CONSTRAINT [PK_UserLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Application] ON 

INSERT [dbo].[Application] ([Id], [AppName], [LogoUrl], [Slogon], [ValidateDate], [Status]) VALUES (1, N'DentalCorner', N'dental.jpg', N'Rifat Abdullah', CAST(N'2016-08-26' AS Date), 1)
SET IDENTITY_INSERT [dbo].[Application] OFF
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId], [BrandName]) VALUES (1, 0, 0, CAST(N'2017-06-28 00:00:00.000' AS DateTime), CAST(N'2017-06-28 00:00:00.000' AS DateTime), 1, 0, N'Fast Food', N'Fixed Category', 0, NULL, NULL)
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId], [BrandName]) VALUES (4, 0, 0, CAST(N'2017-06-28 00:00:00.000' AS DateTime), CAST(N'2017-06-28 00:00:00.000' AS DateTime), 1, 0, N'Kabab', N'Fixed Category', 0, NULL, NULL)
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId], [BrandName]) VALUES (6, 0, 0, CAST(N'2017-08-05 00:00:00.000' AS DateTime), CAST(N'2017-08-05 00:00:00.000' AS DateTime), 1, 0, N'Raw Food', N'Fixed Category', 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[Currency] ON 

INSERT [dbo].[Currency] ([ID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status], [CurrencyName], [ShortName], [Symbol], [Country], [ConversionRate]) VALUES (1, 1, CAST(N'2017-05-08' AS Date), 1, CAST(N'2017-05-08' AS Date), 1, N'Taka', N'BDT', N'?', N'Bangladesh', CAST(1.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[Currency] OFF
SET IDENTITY_INSERT [dbo].[GroupHeader] ON 

INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (1, 1, 1, CAST(N'2016-07-17 02:35:39.107' AS DateTime), CAST(N'2016-07-17 02:35:39.260' AS DateTime), 1, 0, N'Fixed Assets', NULL, 17, 24, 1)
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (2, 1, 1, CAST(N'2016-07-17 02:36:23.837' AS DateTime), CAST(N'2016-07-17 02:36:23.837' AS DateTime), 1, 0, N'Current Assets', NULL, 17, 24, 1)
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (3, 1, 1, CAST(N'2016-07-17 02:36:32.543' AS DateTime), CAST(N'2016-07-17 02:36:32.543' AS DateTime), 1, 0, N' Liabilities', NULL, 18, 25, 1)
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (4, 1, 1, CAST(N'2016-07-17 02:36:40.777' AS DateTime), CAST(N'2016-07-17 02:36:40.777' AS DateTime), 1, 0, N'Equity', NULL, 18, 26, 1)
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (5, 1, 1, CAST(N'2016-07-17 02:36:48.910' AS DateTime), CAST(N'2016-07-17 02:36:48.910' AS DateTime), 1, 0, N'Revenue', NULL, 18, 27, 1)
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (6, 1, 1, CAST(N'2016-07-17 02:36:56.413' AS DateTime), CAST(N'2016-07-17 02:36:56.413' AS DateTime), 1, 0, N'Expense', NULL, 17, 28, 1)
SET IDENTITY_INSERT [dbo].[GroupHeader] OFF
SET IDENTITY_INSERT [dbo].[InvoiceDetails] ON 

INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice], [ProductSerial], [Warranty], [StockIn], [ConversionRate], [Currency]) VALUES (1, 2, 2, CAST(N'2017-08-16 00:00:00.000' AS DateTime), CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, 0, 1, 1, CAST(70.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), CAST(3500.00 AS Decimal(18, 2)), 37, CAST(70.00 AS Decimal(18, 2)), N'', 0, 0, CAST(0.00 AS Decimal(18, 2)), 0)
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice], [ProductSerial], [Warranty], [StockIn], [ConversionRate], [Currency]) VALUES (2, 2, 2, CAST(N'2017-08-16 00:00:00.000' AS DateTime), CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, 0, 1, 2, CAST(50.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), 37, CAST(50.00 AS Decimal(18, 2)), N'', 0, 0, CAST(0.00 AS Decimal(18, 2)), 0)
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice], [ProductSerial], [Warranty], [StockIn], [ConversionRate], [Currency]) VALUES (3, 2, 2, CAST(N'2017-08-16 00:00:00.000' AS DateTime), CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, 0, 1, 26, CAST(50.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), CAST(2500.00 AS Decimal(18, 2)), 37, CAST(50.00 AS Decimal(18, 2)), N'', 0, 0, CAST(0.00 AS Decimal(18, 2)), 0)
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice], [ProductSerial], [Warranty], [StockIn], [ConversionRate], [Currency]) VALUES (4, 2, 2, CAST(N'2017-08-16 00:00:00.000' AS DateTime), CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, 0, 1, 41, CAST(130.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), CAST(6500.00 AS Decimal(18, 2)), 37, CAST(130.00 AS Decimal(18, 2)), N'', 0, 0, CAST(0.00 AS Decimal(18, 2)), 0)
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice], [ProductSerial], [Warranty], [StockIn], [ConversionRate], [Currency]) VALUES (5, 2, 2, CAST(N'2017-08-16 00:00:00.000' AS DateTime), CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, 0, 1, 48, CAST(30.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), CAST(1500.00 AS Decimal(18, 2)), 37, CAST(30.00 AS Decimal(18, 2)), N'', 0, 0, CAST(0.00 AS Decimal(18, 2)), 0)
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice], [ProductSerial], [Warranty], [StockIn], [ConversionRate], [Currency]) VALUES (6, 2, 2, CAST(N'2017-08-16 00:00:00.000' AS DateTime), CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, 0, 1, 49, CAST(40.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), 37, CAST(40.00 AS Decimal(18, 2)), N'', 0, 0, CAST(0.00 AS Decimal(18, 2)), 0)
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice], [ProductSerial], [Warranty], [StockIn], [ConversionRate], [Currency]) VALUES (7, 2, 2, CAST(N'2017-08-16 00:00:00.000' AS DateTime), CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, 0, 1, 50, CAST(1.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), CAST(50.00 AS Decimal(18, 2)), 37, CAST(1.00 AS Decimal(18, 2)), N'', 0, 0, CAST(0.00 AS Decimal(18, 2)), 0)
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice], [ProductSerial], [Warranty], [StockIn], [ConversionRate], [Currency]) VALUES (8, 2, 2, CAST(N'2017-08-16 00:00:00.000' AS DateTime), CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, 0, 2, 1, CAST(70.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(350.00 AS Decimal(18, 2)), 2, CAST(70.00 AS Decimal(18, 2)), N'', 0, 0, CAST(0.00 AS Decimal(18, 2)), 0)
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice], [ProductSerial], [Warranty], [StockIn], [ConversionRate], [Currency]) VALUES (9, 2, 2, CAST(N'2017-08-16 00:00:00.000' AS DateTime), CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, 0, 2, 2, CAST(50.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(250.00 AS Decimal(18, 2)), 2, CAST(50.00 AS Decimal(18, 2)), N'', 0, 0, CAST(0.00 AS Decimal(18, 2)), 0)
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice], [ProductSerial], [Warranty], [StockIn], [ConversionRate], [Currency]) VALUES (10, 2, 2, CAST(N'2017-08-16 00:00:00.000' AS DateTime), CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, 0, 2, 49, CAST(40.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), 2, CAST(40.00 AS Decimal(18, 2)), N'', 0, 0, CAST(0.00 AS Decimal(18, 2)), 0)
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice], [ProductSerial], [Warranty], [StockIn], [ConversionRate], [Currency]) VALUES (11, 2, 2, CAST(N'2017-08-16 00:00:00.000' AS DateTime), CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, 0, 2, 48, CAST(30.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), 2, CAST(30.00 AS Decimal(18, 2)), N'', 0, 0, CAST(0.00 AS Decimal(18, 2)), 0)
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice], [ProductSerial], [Warranty], [StockIn], [ConversionRate], [Currency]) VALUES (12, 2, 2, CAST(N'2017-08-16 00:00:00.000' AS DateTime), CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, 0, 2, 41, CAST(130.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(650.00 AS Decimal(18, 2)), 2, CAST(130.00 AS Decimal(18, 2)), N'', 0, 0, CAST(0.00 AS Decimal(18, 2)), 0)
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice], [ProductSerial], [Warranty], [StockIn], [ConversionRate], [Currency]) VALUES (13, 10006, 10006, CAST(N'2017-08-16 00:00:00.000' AS DateTime), CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, 0, 3, 1, CAST(70.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(700.00 AS Decimal(18, 2)), 38, CAST(70.00 AS Decimal(18, 2)), N'', 0, 0, CAST(0.00 AS Decimal(18, 2)), 0)
SET IDENTITY_INSERT [dbo].[InvoiceDetails] OFF
SET IDENTITY_INSERT [dbo].[InvoiceInfo] ON 

INSERT [dbo].[InvoiceInfo] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceNo], [PartyId], [SubTotal], [ServiceCharge], [Discount], [Vat], [GrandTotal], [InvoiceReferenceId], [PropertyId], [Remarks], [InvoiceDate], [DebitId], [CreditId], [PartyName], [PartyAddress], [ProcessBy], [ApprovedBy], [ReferenceNo], [ConversionRate], [Currency]) VALUES (1, 2, 2, CAST(N'2017-08-16 00:00:00.000' AS DateTime), CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, 0, N'Entry-1', 14, CAST(18550 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0 AS Decimal(18, 0)), NULL, 37, NULL, CAST(N'2017-08-16 11:44:57.510' AS DateTime), 0, 0, NULL, NULL, 2, 2, N'', CAST(0.00 AS Decimal(18, 2)), 0)
INSERT [dbo].[InvoiceInfo] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceNo], [PartyId], [SubTotal], [ServiceCharge], [Discount], [Vat], [GrandTotal], [InvoiceReferenceId], [PropertyId], [Remarks], [InvoiceDate], [DebitId], [CreditId], [PartyName], [PartyAddress], [ProcessBy], [ApprovedBy], [ReferenceNo], [ConversionRate], [Currency]) VALUES (2, 2, 2, CAST(N'2017-08-16 00:00:00.000' AS DateTime), CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, 0, N'1', 14, CAST(1600 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1600 AS Decimal(18, 0)), NULL, 2, NULL, CAST(N'2017-08-16 11:46:01.667' AS DateTime), 0, 1, NULL, NULL, 2, 2, N'', CAST(0.00 AS Decimal(18, 2)), 0)
INSERT [dbo].[InvoiceInfo] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceNo], [PartyId], [SubTotal], [ServiceCharge], [Discount], [Vat], [GrandTotal], [InvoiceReferenceId], [PropertyId], [Remarks], [InvoiceDate], [DebitId], [CreditId], [PartyName], [PartyAddress], [ProcessBy], [ApprovedBy], [ReferenceNo], [ConversionRate], [Currency]) VALUES (3, 10006, 10006, CAST(N'2017-08-16 00:00:00.000' AS DateTime), CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, 0, N'Entry-1', 14, CAST(700 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0 AS Decimal(18, 0)), NULL, 38, NULL, CAST(N'2017-08-16 11:54:02.950' AS DateTime), 0, 0, NULL, NULL, 10006, 10006, N'', CAST(0.00 AS Decimal(18, 2)), 0)
SET IDENTITY_INSERT [dbo].[InvoiceInfo] OFF
SET IDENTITY_INSERT [dbo].[Ledger] ON 

INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (1, 3, 3, CAST(N'2017-02-22 00:51:46.450' AS DateTime), CAST(N'2017-02-22 00:51:46.450' AS DateTime), 1, 0, N'Sales', 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (2, 3, 3, CAST(N'2017-02-22 00:52:01.147' AS DateTime), CAST(N'2017-02-22 00:52:01.147' AS DateTime), 1, 0, N'Purchase', 4, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (3, 3, 3, CAST(N'2017-02-22 00:52:12.483' AS DateTime), CAST(N'2017-02-22 00:52:12.483' AS DateTime), 1, 0, N'Current Stock', 5, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (4, 3, 3, CAST(N'2017-02-22 01:09:57.753' AS DateTime), CAST(N'2017-02-22 01:12:03.123' AS DateTime), 1, 0, N'Bonus Income', 7, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (5, 3, 3, CAST(N'2017-02-22 01:12:42.950' AS DateTime), CAST(N'2017-02-22 01:12:42.950' AS DateTime), 1, 0, N'Product Expire Expense', 8, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (6, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, N'Profit Income', 6, NULL, 0, N'asdasd', NULL, NULL, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (7, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, N'Purchase Discount', 8, NULL, 0, N'dfgdfg', NULL, NULL, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (8, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, N'Sales Discount', 7, NULL, 0, N'dfgdfg', NULL, NULL, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (9, 0, 0, CAST(N'2017-05-17 00:00:00.000' AS DateTime), CAST(N'2017-05-17 00:00:00.000' AS DateTime), 1, 0, N'Purchase Vat', 8, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (10, 0, 0, CAST(N'2017-05-17 00:00:00.000' AS DateTime), CAST(N'2017-05-17 00:00:00.000' AS DateTime), 1, 0, N'Sales Vat', 7, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (11, 0, 0, CAST(N'2017-05-17 00:00:00.000' AS DateTime), CAST(N'2017-05-17 00:00:00.000' AS DateTime), 1, 0, N'Purchase Service Charge', 8, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (12, 0, 0, CAST(N'2017-05-17 00:00:00.000' AS DateTime), CAST(N'2017-05-17 00:00:00.000' AS DateTime), 1, 0, N'Sales Service Charge', 7, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (14, 0, 0, CAST(N'2017-05-17 00:00:00.000' AS DateTime), CAST(N'2017-05-17 00:00:00.000' AS DateTime), 1, 0, N'Wow Food', 1, NULL, 13, N'Wow Food', N'himel', N'37 adabor', NULL, N'01671182591', NULL, N'kzhimel@yahoo.com', 0)
SET IDENTITY_INSERT [dbo].[Ledger] OFF
SET IDENTITY_INSERT [dbo].[LedgerGroup] ON 

INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (1, 3, 3, CAST(N'2017-02-22 00:43:38.843' AS DateTime), CAST(N'2017-02-22 00:43:38.843' AS DateTime), 1, 0, N'Creditors', NULL, NULL, 3, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (2, 3, 3, CAST(N'2017-02-22 00:43:56.537' AS DateTime), CAST(N'2017-02-22 00:43:56.537' AS DateTime), 1, 0, N'Debtors', NULL, NULL, 2, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (3, 3, 3, CAST(N'2017-02-22 00:44:33.793' AS DateTime), CAST(N'2017-02-22 00:44:33.793' AS DateTime), 1, 0, N'Sales', NULL, NULL, 5, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (4, 3, 3, CAST(N'2017-02-22 00:44:50.727' AS DateTime), CAST(N'2017-02-22 00:44:50.727' AS DateTime), 1, 0, N'Purchase', NULL, NULL, 6, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (5, 3, 3, CAST(N'2017-02-22 00:45:14.387' AS DateTime), CAST(N'2017-02-22 00:45:14.387' AS DateTime), 1, 0, N'Current Stock', NULL, NULL, 2, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (6, 3, 3, CAST(N'2017-02-22 00:45:49.553' AS DateTime), CAST(N'2017-02-22 00:45:49.553' AS DateTime), 1, 0, N'Profit and Loss A/C', NULL, NULL, 4, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (7, 3, 3, CAST(N'2017-02-22 01:11:34.557' AS DateTime), CAST(N'2017-02-22 01:11:34.557' AS DateTime), 1, 0, N'Others Revenue', NULL, NULL, 5, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (8, 3, 3, CAST(N'2017-02-22 01:11:51.787' AS DateTime), CAST(N'2017-02-22 01:11:51.787' AS DateTime), 1, 0, N'Others Expense', NULL, NULL, 6, 0)
SET IDENTITY_INSERT [dbo].[LedgerGroup] OFF
SET IDENTITY_INSERT [dbo].[Permission] ON 

INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (1, N'AllAccountController', N'0000001   ', N'Account', N'UserList^SaveUser^DeleteUser^ChangePassword^Login^Logout^PasswordGenerator', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (2, N'AllAccountingController', N'0000002   ', N'Accounting', N'LedgerGroupList^SaveLedgerGroup^DeleteLedgerGroup^LedgerList^SaveLedger^DeleteLedger', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (3, N'AllAccountingReport', N'0000003   ', N'AccountingReport', N'AccountsLedger', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (4, N'AllCategoryController', N'0000004   ', N'Category', N'List^Register^Delete', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (5, N'AllHomeController', N'0000005   ', N'Home', N'Index^AuthFailed', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (6, N'AllInventoryController', N'0000006   ', N'Inventory', N'ProductSettingsList^SaveProductSettings^DeleteProductSettings^StockEntryList^SaveStockEntry^OnChangLoadProduct^LoadCurrentStockAmount^LoadQuantityForEdit^LoadSalesRepresentitive^LoadInvoiceNo^LoadAmountByQuantity', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (7, N'AllPaymentController', N'0000007   ', N'Payment', N'DealerPaymentModule^SupplierPaymentModule^SavePayment^DeletePayment', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (8, N'AllPurchaseController', N'0000008   ', N'Purchase', N'PurchaseEntryList^SavePurchaseEntry^StockOutEntryList^SaveStockOutEntry^GetUpdatedProducts^OnChangLoadProduct^LoadProduct^LoadInvoiceInfoDetail^GetNewInvoiceInfoDetail^GetNewInvoiceInfoDetailBySerial^GetProductListByCategory^DeleteInvoiceInfo', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (9, N'AllReportController', N'0000009   ', N'Report', N'Index^SalesReport^PurchaseReport^InvoiceReport^OpeningStock^ClosingStock^PartyLedger^IncomeStatement^BalanceSheet^TrialBalanceReport^StockListReport', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (10, N'AllSalaryController', N'0000010   ', N'Salary', N'SalaryList^SaveSalary^DeleteSalary^SaveAdvancedSalary^LoadAllowanceByEmployee^CalCulateEmployeePercentage', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (11, N'AllSalesController', N'0000011   ', N'Sales', N'SalesInvoiceList^AddSalesInvoice^AddReturnInvoice^SaveSalesEntry^LoadProductByCategory^LoadProduct^EditInvoiceInfo^PrintBill^LoadPayemntByParty^DeleteInvoiceInfo^GetProductListByCategory^GetNewInvoiceInfoDetail^GetNewInvoiceInfoDetail^GetNewInvoiceInfoDetailByExcel^GetCurrentStockForSales', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (12, N'AllSettingController', N'0000012   ', N'Settings', N'EmployeeList^SaveEmployee^DeleteEmployee^SupplierPartyList^DealerPartyList^SaveParty^DeleteParty^AreaList^SaveArea^DeleteArea^BankList^SaveBank^DeleteBank', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (13, N'AllStockController', N'0000013   ', N'Stock', N'Index^CurrentStockList^GEtProductStock', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (14, N'AllTemplateController', N'0000014   ', N'Template', N'List^Register^Modify^GetPermissionList', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (15, N'AllTransactionController', N'0000015   ', N'Transaction', N'TransactionList^TransactionForm^SaveTransactionForm^DeleteTransaction', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (16, N'AllUserController', N'0000016   ', N'UserCommon', N'List^Register', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (17, N'AllJsonController', N'0000017   ', N'JsonList', N'Index^LoadProductByCategory^LoadProductStock^LoadProductStockSell', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (18, N'InventoryWithoutProduct', N'0000018   ', N'Inventory', N'StockEntryList^SaveStockEntry^OnChangLoadProduct^LoadCurrentStockAmount^LoadQuantityForEdit^LoadSalesRepresentitive^LoadInvoiceNo^LoadAmountByQuantity', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (19, N'AllJobExpenseController', N'0000019   ', N'JobExpense', N'JobExpenseList^SaveExpense^SaveExpense^DeleteExpense^OnChangeLoadDetail', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (20, N'AllowedReport', N'0000020   ', N'Report', N'SalesReport^PurchaseReport^OpeningStock^ClosingStock^PartyLedgerJobReport^JobCostingReport^InvoiceReport', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (21, N'AllTransactionController', N'0000021   ', N'OpeningTransaction', N'TransactionList^TransactionForm^SaveTransactionForm^DeleteTransaction', 1, 1)
SET IDENTITY_INSERT [dbo].[Permission] OFF
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (1, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Dada Lassi', CAST(70.00 AS Decimal(18, 2)), NULL, CAST(70.00 AS Decimal(18, 2)), N'1', N'dada_lassi.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (2, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Pudding (Per Pcs)', CAST(50.00 AS Decimal(18, 2)), NULL, CAST(50.00 AS Decimal(18, 2)), N'2', N'Pudding.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (3, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Boro Naga', CAST(150.00 AS Decimal(18, 2)), NULL, CAST(150.00 AS Decimal(18, 2)), N'3', N'naga_burger.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (4, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Bar-B-Q (Per Pcs)', CAST(120.00 AS Decimal(18, 2)), NULL, CAST(120.00 AS Decimal(18, 2)), N'4', N'bar_b_q_chicken.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (5, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Chicken Burger', CAST(90.00 AS Decimal(18, 2)), NULL, CAST(90.00 AS Decimal(18, 2)), N'5', N'chicken_burger.png')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (6, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Baccha Burger', CAST(70.00 AS Decimal(18, 2)), NULL, CAST(70.00 AS Decimal(18, 2)), N'6', N'baccha_burger.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (7, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Beef Burger With Cheese', CAST(150.00 AS Decimal(18, 2)), NULL, CAST(150.00 AS Decimal(18, 2)), N'7', N'beef_burger_with_cheese.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (8, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Beef Burger Without Cheese', CAST(120.00 AS Decimal(18, 2)), NULL, CAST(120.00 AS Decimal(18, 2)), N'8', N'beef_burger_without_cheese.png')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (9, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Jumbo Burger', CAST(140.00 AS Decimal(18, 2)), NULL, CAST(140.00 AS Decimal(18, 2)), N'9', N'jumbo_burger.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (10, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Special Mushroom Burger', CAST(110.00 AS Decimal(18, 2)), NULL, CAST(110.00 AS Decimal(18, 2)), N'10', N'special_mushroom_burger.png')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (11, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-08-07 00:00:00.000' AS DateTime), 1, 0, 1, N'Vegetable Burger', CAST(70.00 AS Decimal(18, 2)), NULL, CAST(70.00 AS Decimal(18, 2)), N'11', N'vegetable_burger.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (12, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Taco (Per Pcs)', CAST(70.00 AS Decimal(18, 2)), NULL, CAST(70.00 AS Decimal(18, 2)), N'12', N'taco.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (13, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Shwarma', CAST(80.00 AS Decimal(18, 2)), NULL, CAST(80.00 AS Decimal(18, 2)), N'13', N'shwarma.png')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (14, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Fried Chicken (Per Pcs)', CAST(70.00 AS Decimal(18, 2)), NULL, CAST(70.00 AS Decimal(18, 2)), N'14', N'fried_chicken.png')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (15, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Club Sandwich', CAST(50.00 AS Decimal(18, 2)), NULL, CAST(50.00 AS Decimal(18, 2)), N'15', N'club_sandwich.png')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (16, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Chicken Sub Sandwich', CAST(100.00 AS Decimal(18, 2)), NULL, CAST(100.00 AS Decimal(18, 2)), N'16', N'chicken_sub_sandwich.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (17, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'French Fries', CAST(60.00 AS Decimal(18, 2)), NULL, CAST(60.00 AS Decimal(18, 2)), N'17', N'french_fries.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (18, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Special Nachos', CAST(140.00 AS Decimal(18, 2)), NULL, CAST(140.00 AS Decimal(18, 2)), N'18', N'nachos.png')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (19, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Classic Pizza', CAST(80.00 AS Decimal(18, 2)), NULL, CAST(80.00 AS Decimal(18, 2)), N'19', N'classic_pizza.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (20, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Fried Wings (6 Pcs)', CAST(140.00 AS Decimal(18, 2)), NULL, CAST(140.00 AS Decimal(18, 2)), N'20', N'fried_wings.png')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (21, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Regular Hot Coffee', CAST(30.00 AS Decimal(18, 2)), NULL, CAST(30.00 AS Decimal(18, 2)), N'21', N'hot_coffee.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (22, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Sunny Dale', CAST(100.00 AS Decimal(18, 2)), NULL, CAST(100.00 AS Decimal(18, 2)), N'22', N'sunny_dale.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (23, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Oreo Shake', CAST(100.00 AS Decimal(18, 2)), NULL, CAST(100.00 AS Decimal(18, 2)), N'23', N'oreo_shake.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (24, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Strawberry Shake', CAST(70.00 AS Decimal(18, 2)), NULL, CAST(70.00 AS Decimal(18, 2)), N'24', N'strawberry_shake.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (25, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Blizzard', CAST(100.00 AS Decimal(18, 2)), NULL, CAST(100.00 AS Decimal(18, 2)), N'25', N'blizzard.png')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (26, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Cold Coffee', CAST(50.00 AS Decimal(18, 2)), NULL, CAST(50.00 AS Decimal(18, 2)), N'26', N'cold_coffee.png')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (27, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Mineral Water (500ml)', CAST(15.00 AS Decimal(18, 2)), NULL, CAST(15.00 AS Decimal(18, 2)), N'27', N'water.png')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (28, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Soft Drinks (Per Glass)', CAST(30.00 AS Decimal(18, 2)), NULL, CAST(30.00 AS Decimal(18, 2)), N'28', N'soft_drinks.png')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (29, 4, 4, CAST(N'2017-07-20 00:00:00.000' AS DateTime), CAST(N'2017-07-20 00:00:00.000' AS DateTime), 1, 0, 1, N'Luci (Per Pcs)', CAST(10.00 AS Decimal(18, 2)), NULL, CAST(10.00 AS Decimal(18, 2)), N'29', N'luci.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (30, 4, 4, CAST(N'2017-07-27 00:00:00.000' AS DateTime), CAST(N'2017-07-27 00:00:00.000' AS DateTime), 1, 0, 1, N'Rumali Ruti (Per Pcs)', CAST(20.00 AS Decimal(18, 2)), NULL, CAST(20.00 AS Decimal(18, 2)), N'30', N'rumali_ruti.png')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (31, 4, 4, CAST(N'2017-07-27 00:00:00.000' AS DateTime), CAST(N'2017-07-27 00:00:00.000' AS DateTime), 1, 0, 1, N'Chola Bhature', CAST(120.00 AS Decimal(18, 2)), NULL, CAST(120.00 AS Decimal(18, 2)), N'31', N'chola_bhature.png')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (32, 4, 4, CAST(N'2017-07-27 00:00:00.000' AS DateTime), CAST(N'2017-07-27 00:00:00.000' AS DateTime), 1, 0, 1, N'Chicken Tandoori (1 Pcs)', CAST(110.00 AS Decimal(18, 2)), NULL, CAST(110.00 AS Decimal(18, 2)), N'32', N'chicken_tandoori.png')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (33, 4, 4, CAST(N'2017-07-27 00:00:00.000' AS DateTime), CAST(N'2017-07-27 00:00:00.000' AS DateTime), 1, 0, 1, N'Beef Chap (1 Pcs)', CAST(100.00 AS Decimal(18, 2)), NULL, CAST(100.00 AS Decimal(18, 2)), N'33', N'beef_chap.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (34, 4, 4, CAST(N'2017-07-27 00:00:00.000' AS DateTime), CAST(N'2017-07-27 00:00:00.000' AS DateTime), 1, 0, 1, N'Beef Boti Kabab (1 Plate)', CAST(100.00 AS Decimal(18, 2)), NULL, CAST(100.00 AS Decimal(18, 2)), N'34', N'beef_boti_kabab.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (35, 4, 4, CAST(N'2017-07-27 00:00:00.000' AS DateTime), CAST(N'2017-07-27 00:00:00.000' AS DateTime), 1, 0, 1, N'Chicken Chap', CAST(100.00 AS Decimal(18, 2)), NULL, CAST(100.00 AS Decimal(18, 2)), N'35', N'chicken_chap.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (36, 4, 4, CAST(N'2017-07-27 00:00:00.000' AS DateTime), CAST(N'2017-07-27 00:00:00.000' AS DateTime), 1, 0, 1, N'Chicken Boti Kabab', CAST(110.00 AS Decimal(18, 2)), NULL, CAST(110.00 AS Decimal(18, 2)), N'36', N'chicken_boti_kabab.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (37, 4, 4, CAST(N'2017-07-27 00:00:00.000' AS DateTime), CAST(N'2017-07-27 00:00:00.000' AS DateTime), 1, 0, 1, N'Beef Shik Kabab (1 Shik)', CAST(100.00 AS Decimal(18, 2)), NULL, CAST(100.00 AS Decimal(18, 2)), N'37', N'beef_shik_kabab.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (38, 4, 4, CAST(N'2017-07-27 00:00:00.000' AS DateTime), CAST(N'2017-07-27 00:00:00.000' AS DateTime), 1, 0, 1, N'Beef Lota Kabab (Per Pcs)', CAST(120.00 AS Decimal(18, 2)), NULL, CAST(120.00 AS Decimal(18, 2)), N'38', N'beef_lota_kabab.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (39, 4, 4, CAST(N'2017-07-27 00:00:00.000' AS DateTime), CAST(N'2017-07-27 00:00:00.000' AS DateTime), 1, 0, 1, N'Beef Achari', CAST(150.00 AS Decimal(18, 2)), NULL, CAST(150.00 AS Decimal(18, 2)), N'39', N'beef_achari.jpeg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (40, 4, 4, CAST(N'2017-07-27 00:00:00.000' AS DateTime), CAST(N'2017-07-27 00:00:00.000' AS DateTime), 1, 0, 1, N'Beef Curry', CAST(120.00 AS Decimal(18, 2)), NULL, CAST(120.00 AS Decimal(18, 2)), N'40', N'beef_Curry.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (41, 4, 4, CAST(N'2017-07-27 00:00:00.000' AS DateTime), CAST(N'2017-07-27 00:00:00.000' AS DateTime), 1, 0, 1, N'Chicken Achari', CAST(130.00 AS Decimal(18, 2)), NULL, CAST(130.00 AS Decimal(18, 2)), N'41', N'chicken_achari.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (42, 4, 4, CAST(N'2017-07-27 00:00:00.000' AS DateTime), CAST(N'2017-07-27 00:00:00.000' AS DateTime), 1, 0, 1, N'Chicken Tikka', CAST(100.00 AS Decimal(18, 2)), NULL, CAST(100.00 AS Decimal(18, 2)), N'42', N'chicken_tikka.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (43, 4, 4, CAST(N'2017-08-05 00:00:00.000' AS DateTime), CAST(N'2017-08-05 00:00:00.000' AS DateTime), 1, 0, 1, N'Pepsi (250ml) + Service Charge 5 TK', CAST(20.00 AS Decimal(18, 2)), NULL, CAST(20.00 AS Decimal(18, 2)), N'43', N'pepsi250.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (44, 4, 4, CAST(N'2017-08-05 00:00:00.000' AS DateTime), CAST(N'2017-08-05 00:00:00.000' AS DateTime), 1, 0, 1, N'Pepsi (400ml) + Service Charge 5 TK', CAST(25.00 AS Decimal(18, 2)), NULL, CAST(25.00 AS Decimal(18, 2)), N'44', N'pepsi400.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (45, 4, 4, CAST(N'2017-08-05 00:00:00.000' AS DateTime), CAST(N'2017-08-05 00:00:00.000' AS DateTime), 1, 0, 1, N'Pepsi (500ml) + Service Charge 5 TK', CAST(30.00 AS Decimal(18, 2)), NULL, CAST(30.00 AS Decimal(18, 2)), N'45', N'pepsi500.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (46, 4, 4, CAST(N'2017-08-05 00:00:00.000' AS DateTime), CAST(N'2017-08-05 00:00:00.000' AS DateTime), 1, 0, 1, N'Mirinda (500ml) + Service Charge 5 TK', CAST(35.00 AS Decimal(18, 2)), NULL, CAST(35.00 AS Decimal(18, 2)), N'46', N'mirinda500.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (47, 4, 4, CAST(N'2017-08-05 00:00:00.000' AS DateTime), CAST(N'2017-08-05 00:00:00.000' AS DateTime), 1, 0, 1, N'7-UP (500ml) + Service Charge 5 TK', CAST(35.00 AS Decimal(18, 2)), NULL, CAST(35.00 AS Decimal(18, 2)), N'47', N'7UP500.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (48, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Dew (400ml) + Service Charge 5 TK', CAST(50.00 AS Decimal(18, 2)), NULL, CAST(30.00 AS Decimal(18, 2)), N'48', N'dew400.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (49, 4, 4, CAST(N'2017-07-19 00:00:00.000' AS DateTime), CAST(N'2017-07-19 00:00:00.000' AS DateTime), 1, 0, 1, N'Dew (500ml) + Service Charge 5 TK', CAST(70.00 AS Decimal(18, 2)), NULL, CAST(40.00 AS Decimal(18, 2)), N'49', N'dew500.jpg')
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (50, 4, 4, CAST(N'2017-08-05 00:00:00.000' AS DateTime), CAST(N'2017-08-05 00:00:00.000' AS DateTime), 1, 0, 6, N'Burger Bon', CAST(1.00 AS Decimal(18, 2)), NULL, CAST(1.00 AS Decimal(18, 2)), N'900', NULL)
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (51, 4, 4, CAST(N'2017-08-05 00:00:00.000' AS DateTime), CAST(N'2017-08-05 00:00:00.000' AS DateTime), 1, 0, 6, N'Cheese', CAST(1.00 AS Decimal(18, 2)), NULL, CAST(1.00 AS Decimal(18, 2)), N'901', NULL)
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode], [ImageUrl]) VALUES (52, 4, 4, CAST(N'2017-08-07 00:00:00.000' AS DateTime), CAST(N'2017-08-07 00:00:00.000' AS DateTime), 1, 0, 6, N'Mayonnaise', CAST(1.00 AS Decimal(18, 2)), NULL, CAST(1.00 AS Decimal(18, 2)), N'902', NULL)
SET IDENTITY_INSERT [dbo].[Product] OFF
SET IDENTITY_INSERT [dbo].[TableProperty] ON 

INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (1, N' Ledger ', N'TrialVisible', 1, N' InVisible on Ledger ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (2, N' InvoiceInfo ', N'SalesInvoiceType', 1, N' Sales ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (3, N' InvoiceInfo ', N'SalesInvoiceType', 2, N' SalesReplace ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (4, N' InvoiceInfo ', N'SalesInvoiceType', 3, N' Sales Return ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (5, N' InvoiceInfo ', N'PurchaseInvoiceType', 1, N' Purchase ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (6, N' InvoiceInfo ', N'PurchaseInvoiceType', 2, N' Purchase Return ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (7, N' InvoiceInfo ', N'PurchaseInvoiceType', 3, N'Damage')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (8, N' InvoiceInfo ', N'PurchaseInvoiceType', 4, N' Bonus ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (9, N' Salary ', N'SalaryType', 1, N' Current Salary ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (10, N' Salary ', N'SalaryType', 2, N' Advance Salary ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (11, N' Product ', N'ProductType', 1, N' Finished Product ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (12, N' Product ', N'ProductType', 2, N' Raw Rpoduct ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (13, N' Ledger ', N'PartyType', 1, N' Supplier Party ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (14, N' Ledger ', N'PartyType', 2, N' Client Party ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (15, N' Ledger ', N'PartyType', 3, N' Local Supplier ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (16, N' Ledger ', N'PartyType', 4, N' Local Client ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (17, N' GroupHeader ', N'HeaderType', 1, N' Debit/To ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (18, N' GroupHeader ', N'HeaderType', 2, N' Credit/From ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (19, N' LedgerGroup ', N'GroupType', 1, N' Cash Transaction ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (20, N' LedgerGroup ', N'GroupType', 2, N' Purchase Ldegers ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (21, N' LedgerGroup ', N'GroupType', 3, N' Sales Ledger ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (22, N' LedgerGroup ', N'GroupType', 4, N' SalaryLedger ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (23, N' LedgerGroup ', N'GroupType', 5, N' Inventory ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (24, N' LedgerGroup ', N'GroupType', 6, N' Profit/ Loss ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (25, N' LedgerGroup ', N'GroupType', 7, N' Capital ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (26, N'Payment ', N'PaymentStatus', 1, N'Paid')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (27, N'Payment ', N'PaymentStatus', 2, N'Pending')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (28, N'Payment', N'TransactionType', 1, N'Payment')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (29, N'Payment', N'TransactionType', 2, N'Receive')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (30, N' GroupHeader ', N'AccountsType', 1, N' Asset ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (31, N' GroupHeader ', N'AccountsType', 2, N' Liability ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (32, N' GroupHeader ', N'AccountsType', 3, N' Equity ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (33, N' GroupHeader ', N'AccountsType', 4, N' Revenue ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (34, N' GroupHeader ', N'AccountsType', 5, N' Expense ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (35, N' GroupHeader ', N'AccountsType', 6, N' Drwaings ')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (36, N'InvoiceDetails', N'SalesType', 1, N'Gift')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (37, N'InvoiceDetails', N'StockType', 1, N'Stock In')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (38, N'InvoiceDetails', N'StockType', 2, N'Stock Out')
SET IDENTITY_INSERT [dbo].[TableProperty] OFF
SET IDENTITY_INSERT [dbo].[Template] ON 

INSERT [dbo].[Template] ([Id], [TemplateName], [PermissionIds], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ApplicationId]) VALUES (1, N'Admin', N'1^2^3^4^5^6^7^8^9^10^11^12^13^14^15^16^17^18^19^20^21', 1, 0, CAST(N'2016-08-25' AS Date), 0, CAST(N'2016-09-04' AS Date), 1)
INSERT [dbo].[Template] ([Id], [TemplateName], [PermissionIds], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ApplicationId]) VALUES (2, N'testAdmin', N'1^2^3^4', 1, 0, CAST(N'2016-09-02' AS Date), NULL, NULL, 1)
INSERT [dbo].[Template] ([Id], [TemplateName], [PermissionIds], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ApplicationId]) VALUES (3, N'Salesman', N'11^5', 1, 1, CAST(N'2016-09-02' AS Date), 1, CAST(N'2016-09-02' AS Date), 1)
INSERT [dbo].[Template] ([Id], [TemplateName], [PermissionIds], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ApplicationId]) VALUES (4, N'Manager', N'11^5^8', 1, 1, CAST(N'2017-07-08' AS Date), NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Template] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [Name], [IsCreator]) VALUES (1, N'root', N'root', 1, 1, CAST(N'2017-08-08 00:00:00.000' AS DateTime), 1, NULL, 1, CAST(N'2016-03-24' AS Date), NULL, NULL, 1, 1, 1, NULL, N'Admin', 1)
INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [Name], [IsCreator]) VALUES (2, N'wowfood', N'123456', 1, 1, CAST(N'2017-08-10 00:00:00.000' AS DateTime), 1, NULL, 1, CAST(N'2016-03-28' AS Date), NULL, NULL, 1, 1, 1, NULL, N'Owner', 1)
INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [Name], [IsCreator]) VALUES (4, N'Manager', N'mg123456', 1, 1, CAST(N'2017-08-10 00:00:00.000' AS DateTime), 1, NULL, 0, CAST(N'2016-09-01' AS Date), NULL, NULL, 4, 1, 1, NULL, N'Manager', NULL)
INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [Name], [IsCreator]) VALUES (5, N'Salesman', N'sm123456', 1, 0, CAST(N'2017-08-10 00:00:00.000' AS DateTime), 1, NULL, 1, CAST(N'2017-07-08' AS Date), NULL, NULL, 3, 1, 1, NULL, N'Salesman', NULL)
INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [Name], [IsCreator]) VALUES (10006, N'admin', N'admin', 1, 1, CAST(N'2017-08-16 00:00:00.000' AS DateTime), 1, NULL, 1, CAST(N'2017-08-08' AS Date), NULL, NULL, 1, 1, 1, NULL, N'Admin', NULL)
SET IDENTITY_INSERT [dbo].[User] OFF
USE [master]
GO
ALTER DATABASE [RPOS] SET  READ_WRITE 
GO
